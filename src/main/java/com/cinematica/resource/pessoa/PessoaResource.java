package com.cinematica.resource.pessoa;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.framework.consulta.restricao.Restricoes;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.model.pessoa.Pessoa;
import com.cinematica.service.pessoa.PessoaService;

@RestController
@RequestMapping("paciente")
@Transactional
@CrossOrigin
public class PessoaResource {

	@Autowired
	private PessoaService pessoaService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESSOA_CONSULTAR')")
	public List<Pessoa> listarTodos(Pessoa entidade){
		Pessoa pessoaConsulta = new Pessoa();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(VerificadorUtil.naoEstaNulo(entidade.getNome())) {
			listaRestricoes.add(Restricoes.like("nome", entidade.getNome()));
		}
		return this.pessoaService.filtro(listaRestricoes, pessoaConsulta, pessoaConsulta.getCampoOrderBy());
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ROLE_PESSOA_INSERIR')")
	public void cadastrar(@Valid @RequestBody Pessoa entidade) {
		this.pessoaService.cadastrar(entidade);
	}
	
	@GetMapping("/{id}")
	public Pessoa consultarPorId(@PathVariable Integer id) {
		return this.pessoaService.consultarPorId(id);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESSOA_EXCLUIR')")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer id) {
		this.pessoaService.excluir(this.consultarPorId(id));
	}
	
	@PutMapping
	public void alterar(@Valid @RequestBody Pessoa entidade){
		this.pessoaService.alterar(entidade);
		/*BeanUtils.copyProperties(entidade, pessoaSalva, "id");*/
	}
}
