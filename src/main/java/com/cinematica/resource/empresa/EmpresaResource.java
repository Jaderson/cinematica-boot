package com.cinematica.resource.empresa;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.exception.CinematicaException;
import com.cinematica.exception.CinematicaExceptionHandler.Erro;
import com.cinematica.framework.json.JsonUtil;
import com.cinematica.model.empresa.Empresa;
import com.cinematica.security.config.RecursoCriadoEvent;
import com.cinematica.service.empresa.EmpresaService;

@RestController
@RequestMapping("/empresa")
@Transactional
public class EmpresaResource {
	
	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource messageSource;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_EMPRESA_CONSULTAR')")
	public ResponseEntity<String> listarTodos() {
		List<Empresa> listEmpresa = this.empresaService.consultarTodos(new Empresa());
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listEmpresa);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ROLE_EMPRESA_INSERIR')")
	public ResponseEntity<String> cadastrar(@Valid @RequestBody Empresa entidade, HttpServletResponse response) {
		this.empresaService.cadastrar(entidade);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, entidade.getId()));
		return new ResponseEntity<>(jsonObject, HttpStatus.CREATED);
	}


	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_EMPRESA_CONSULTAR')")
	public ResponseEntity<String> consultarPorId(@PathVariable Integer id) {
		Empresa entidade = this.empresaService.consultarPorId(id);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}
	

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_EMPRESA_EXCLUIR')")
	public ResponseEntity<String> excluir(@PathVariable("id") Integer id) {
		Empresa entidade = this.empresaService.consultarPorId(id);
		this.empresaService.excluir(entidade);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<String>(jsonObject, HttpStatus.OK);
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_EMPRESA_ALTERAR')")
	public ResponseEntity<String> atualizar(@RequestBody Empresa entidade) {

		Empresa empresa = this.empresaService.consultarPorId(entidade.getId());
		empresa.setCnpj(entidade.getCnpj());
		empresa.setDataContratacao(entidade.getDataContratacao());
		empresa.setEmail(entidade.getEmail());
		/*empresa.setEndereco(endereco);*/
		empresa.setFax(entidade.getFax());
		empresa.setInscricaoEstadual(entidade.getInscricaoEstadual());
		empresa.setNomeFantasia(entidade.getNomeFantasia());
		empresa.setTelefone(entidade.getTelefone());
		empresa.setWebsite(entidade.getWebsite());
		empresa.setRazaoSocial(entidade.getRazaoSocial());

		this.empresaService.alterar(empresa);
		String jsonObject = JsonUtil.convertObjectToJsonString(empresa);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}
	
	@ExceptionHandler({ CinematicaException.class })
	public ResponseEntity<Object> CinematicaException(CinematicaException ex) {
		String mensagemUsuario = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}


}
