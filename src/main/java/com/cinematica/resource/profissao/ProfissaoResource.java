package com.cinematica.resource.profissao;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.exception.CinematicaException;
import com.cinematica.exception.CinematicaExceptionHandler.Erro;
import com.cinematica.framework.json.JsonUtil;
import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.model.profissao.Profissao;
import com.cinematica.security.config.RecursoCriadoEvent;
import com.cinematica.service.profissao.ProfissaoService;

@RestController
@RequestMapping("/profissao")
@Transactional
public class ProfissaoResource {

	@Autowired
	private ProfissaoService profissaoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource messageSource;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_PROFISSAO_CONSULTAR')")
	public ResponseEntity<String> listarTodos() {
		List<Profissao> listProfissao = this.profissaoService.consultarTodos(new Profissao());
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listProfissao);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ROLE_PROFISSAO_INSERIR')")
	public ResponseEntity<String> cadastrar(@Valid @RequestBody Profissao entidade, HttpServletResponse response) {
		this.profissaoService.cadastrar(entidade);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, entidade.getId()));
		return new ResponseEntity<>(jsonObject, HttpStatus.CREATED);
	}


	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_PROFISSAO_CONSULTAR')")
	public ResponseEntity<String> consultarPorId(@PathVariable Integer id) {
		Profissao entidade = this.profissaoService.consultarPorId(id);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}
	

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PROFISSAO_EXCLUIR')")
	public ResponseEntity<String> excluir(@PathVariable("id") Integer id) {
		Profissao entidade = this.profissaoService.consultarPorId(id);
		this.profissaoService.excluir(entidade);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<String>(jsonObject, HttpStatus.OK);
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_PROFISSAO_ALTERAR')")
	public ResponseEntity<String> atualizar(@RequestBody Especialidade entidade) {

		Profissao profissao = this.profissaoService.consultarPorId(entidade.getId());
		profissao.setDescricao(entidade.getDescricao());

		this.profissaoService.alterar(profissao);
		String jsonObject = JsonUtil.convertObjectToJsonString(profissao);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}
	
	@ExceptionHandler({ CinematicaException.class })
	public ResponseEntity<Object> CinematicaException(CinematicaException ex) {
		String mensagemUsuario = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}

}
