package com.cinematica.resource.agenda;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.service.horarioDisponivel.HorarioDisponivelService;

@RestController
@RequestMapping("/horarioDisponivel")
@Transactional
public class HorarioDisponivelResouce {
	
	@Autowired
	private HorarioDisponivelService horarioDisponivelService;

	@GetMapping
	public List<HorarioDisponivel> listarTodos() {
		return this.horarioDisponivelService.consultarTodos(new HorarioDisponivel());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void cadastrar(@Valid @RequestBody HorarioDisponivel entidade) {
		this.horarioDisponivelService.cadastrar(entidade);
	}

	@GetMapping("/{id}")
	public HorarioDisponivel consultarPorId(@PathVariable Integer id) {
		return this.horarioDisponivelService.consultarPorId(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer id) {
		this.horarioDisponivelService.excluir(this.consultarPorId(id));
	}

}
