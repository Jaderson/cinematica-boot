package com.cinematica.resource.agenda;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.model.agenda.Horario;
import com.cinematica.service.horario.HorarioService;

@RestController
@RequestMapping("/horario")
@Transactional
public class HorarioResource {
	@Autowired
	private HorarioService horarioService;

	@GetMapping
	public List<Horario> listarTodos() {
		return this.horarioService.consultarTodos(new Horario());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void cadastrar(@Valid @RequestBody Horario entidade) {
		this.horarioService.cadastrar(entidade);
	}

	@GetMapping("/{id}")
	public Horario consultarPorId(@PathVariable Integer id) {
		return this.horarioService.consultarPorId(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer id) {
		this.horarioService.excluir(this.consultarPorId(id));
	}
}
