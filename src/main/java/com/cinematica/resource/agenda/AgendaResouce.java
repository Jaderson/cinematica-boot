package com.cinematica.resource.agenda;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.model.agenda.Agenda;
import com.cinematica.service.agenda.AgendaService;

@RestController
@RequestMapping("/agenda")
@Transactional
public class AgendaResouce {

	@Autowired
	private AgendaService agendaService;

	@GetMapping
	public List<Agenda> listarTodos() {
		return this.agendaService.consultarTodos(new Agenda());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void cadastrar(@Valid @RequestBody Agenda entidade) {
		this.agendaService.cadastrar(entidade);
	}

	@GetMapping("/{id}")
	public Agenda consultarPorId(@PathVariable Integer id) {
		return this.agendaService.consultarPorId(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer id) {
		this.agendaService.excluir(this.consultarPorId(id));
	}
}
