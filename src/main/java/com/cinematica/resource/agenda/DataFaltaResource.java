package com.cinematica.resource.agenda;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.model.agenda.DataFalta;
import com.cinematica.service.dataFalta.DataFaltaService;

@RestController
@RequestMapping("/dataFalta")
@Transactional
public class DataFaltaResource {
	
	@Autowired
	private DataFaltaService dataFaltaService;

	@GetMapping
	public List<DataFalta> listarTodos() {
		return this.dataFaltaService.consultarTodos(new DataFalta());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void cadastrar(@Valid @RequestBody DataFalta entidade) {
		this.dataFaltaService.cadastrar(entidade);
	}

	@GetMapping("/{id}")
	public DataFalta consultarPorId(@PathVariable Integer id) {
		return this.dataFaltaService.consultarPorId(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer id) {
		this.dataFaltaService.excluir(this.consultarPorId(id));
	}
}
