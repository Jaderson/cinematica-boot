package com.cinematica.email.impl;

import org.springframework.stereotype.Repository;

@Repository
public class EnviadorEmailNovoUsuario {/* implements EnviadorEmail, Runnable{

	private static final String UTF8 = "UTF-8";

	private JavaMailSender mailSender;
	
	private MimeMessagePreparator preparator;
	private VelocityEngine velocityEngine;
	
	@Autowired
	public EnviadorEmailNovoUsuario(JavaMailSender mailSender,  VelocityEngine velocityEngine) {
		this.mailSender = mailSender;
		this.velocityEngine = velocityEngine;
	}

	@Override
	public void enviarEmail(Object... parameters) {
		Usuario usuario = (Usuario) parameters[0];
		String senha = (String) parameters[1];
		assertUsuarioTemEmail(usuario);
		enviarEmail(usuario, usuario.getPessoa().getEmail(), senha);
	}
	
	public void enviarNovaSenha(Object... parameters) {
		Usuario usuario = (Usuario) parameters[0];
		String novaSenha = (String) parameters[1];
		assertUsuarioTemEmail(usuario);
		enviarEmail(usuario, usuario.getPessoa().getEmail(), novaSenha);
	}
	
	public void enviarEmailNovaEmpresa(Usuario usuarioAdminNovaEmpresa, Usuario usuarioLogado, String senha) {
		assertUsuarioTemEmail(usuarioLogado);
		enviarEmail(usuarioAdminNovaEmpresa, usuarioLogado.getPessoa().getEmail(), senha);
	}
	
	@Override
	public void run() {
		mailSender.send(preparator);
	}

	private void enviarEmail(final Usuario dadosUsuario, final String email, final String senha) {
		preparator = new MimeMessagePreparator() {
			
			public void prepare(MimeMessage mimeMessage) throws Exception {
				String template = criarTemplate(dadosUsuario, senha);
				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom("suporte@cinematicaesportiva.com.br");
				mimeMessageHelper.setTo(email);
				mimeMessageHelper.setSubject("NOVO USUÁRIO");
				mimeMessageHelper.setText(template, true);
			};
		};

		new Thread(this).start();
	}

	private String criarTemplate(Usuario dadosUsuario, String senha) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("login", dadosUsuario.getLogin());
		model.put("senha", senha);
		model.put("empresa", dadosUsuario.getEmpresa().getNomeFantasia());
		String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "messages/novo_usuario_email.vm", UTF8, model);
		return template;
	}

	private void assertUsuarioTemEmail(Usuario usuario) {
		if (VerificadorUtil.estaNuloOuVazio(usuario.getPessoa().getEmail())) {
			throw new RuntimeException("Usuário Cadastrado não possui email.");
		}
	}
				*/
}
