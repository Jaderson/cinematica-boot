package com.cinematica.security.config;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.model.autenticacao.UsuarioPerfil;
import com.cinematica.repository.perfilPermissao.PerfilPermissaoRepository;
import com.cinematica.repository.usuario.UsuarioRepository;
import com.cinematica.repository.usuarioPerfil.UsuarioPerfilRepository;

@Service
public class AppUserDetailService implements UserDetailsService{

	@Autowired private UsuarioRepository usuarioRepository;
	@Autowired private UsuarioPerfilRepository usuarioPerfilRepository;
	@Autowired private PerfilPermissaoRepository perfilPermissaoRepository;
	private static final String PREFIXO_PERMISSAO = "ROLE_";
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Set<UsuarioPerfil> setUsuarioPerfil =  new HashSet<UsuarioPerfil>();
		
		
		Usuario usuario  = this.usuarioRepository.consultarUsuarioPorLogin(username);
		List<UsuarioPerfil> usuarioPerfil = this.usuarioPerfilRepository.consultarPorIdUsuario(usuario);
		usuarioPerfil.forEach(UserPerfil -> setUsuarioPerfil.add(UserPerfil));
		usuario.setPerfils(setUsuarioPerfil);
		
		
		if(VerificadorUtil.estaNulo(usuario)) {
			new UsernameNotFoundException("Usuário e/ou senha incorreta");
		}
		return new User(usuario.getPessoa().getNome(), usuario.getSenha(), getPermissao(usuario));
	}

	private Collection<? extends GrantedAuthority> getPermissao(Usuario usuario) {
		Set<PerfilPermissao> setPermissoes = new HashSet<PerfilPermissao>();
		Set<SimpleGrantedAuthority> authorities =  new HashSet<>();
		
		for (UsuarioPerfil usuarioPerfil : usuario.getPerfils()) {
			this.perfilPermissaoRepository.consultarPerfilPermissaoPorPerfil(usuarioPerfil.getPerfil()).forEach(perfil -> setPermissoes.add(perfil));
		}
		setPermissoes.forEach(permissao -> authorities.add(new SimpleGrantedAuthority(PREFIXO_PERMISSAO + permissao.getPermissao().getDescricaoSistema().toUpperCase())));
		return authorities;
	}

}
