package com.cinematica.facade.horario;

import java.util.List;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.agenda.Horario;
import com.cinematica.model.agenda.HorarioDisponivel;

public interface HorarioFacade extends Facade<Horario>{

	List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade);

}
