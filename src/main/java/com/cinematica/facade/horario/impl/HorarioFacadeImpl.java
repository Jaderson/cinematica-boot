package com.cinematica.facade.horario.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.horario.HorarioFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.agenda.Horario;
import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.service.horario.HorarioService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class HorarioFacadeImpl extends AbstractFacade<Horario> implements HorarioFacade{

	private HorarioService horarioService;

	@Autowired
	public HorarioFacadeImpl(HorarioService horarioService) {
		this.horarioService = horarioService;
	}
	
	@Override
	protected Service<Horario> getService() {
		return this.horarioService;
	}

	@Override
	public List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade) {
		return this.horarioService.consultarHorarioPorConfiguracao(entidade);
	}

}
