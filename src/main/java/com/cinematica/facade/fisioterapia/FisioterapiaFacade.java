package com.cinematica.facade.fisioterapia;

import com.cinematica.framework.avaliacoes.Fisioterapia;
import com.cinematica.framework.facade.Facade;

public interface FisioterapiaFacade extends Facade<Fisioterapia>{

}
