package com.cinematica.facade.banco;

import com.cinematica.framework.banco.Banco;
import com.cinematica.framework.facade.Facade;

public interface BancoFacade extends Facade<Banco>{

}
