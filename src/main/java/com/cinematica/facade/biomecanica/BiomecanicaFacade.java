package com.cinematica.facade.biomecanica;

import com.cinematica.framework.avaliacoes.Biomecanica;
import com.cinematica.framework.facade.Facade;

public interface BiomecanicaFacade extends Facade<Biomecanica>{

}
