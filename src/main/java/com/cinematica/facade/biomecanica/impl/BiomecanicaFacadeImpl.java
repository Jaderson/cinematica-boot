package com.cinematica.facade.biomecanica.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.biomecanica.BiomecanicaFacade;
import com.cinematica.framework.avaliacoes.Biomecanica;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.service.biomecanica.BiomecanicaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class BiomecanicaFacadeImpl extends AbstractFacade<Biomecanica> implements BiomecanicaFacade{

	private BiomecanicaService biomecanicaService;

	@Autowired
	public BiomecanicaFacadeImpl(BiomecanicaService biomecanicaService) {
		this.biomecanicaService = biomecanicaService;
	}
	
	@Override
	protected Service<Biomecanica> getService() {
		return this.biomecanicaService;
	}

}
