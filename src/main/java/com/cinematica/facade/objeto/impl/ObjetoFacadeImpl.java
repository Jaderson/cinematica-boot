package com.cinematica.facade.objeto.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.objeto.ObjetoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Objeto;
import com.cinematica.service.objeto.ObjetoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class ObjetoFacadeImpl extends AbstractFacade<Objeto> implements ObjetoFacade{

	private ObjetoService objetoService;

	@Autowired
	public ObjetoFacadeImpl(ObjetoService objetoService) {
		this.objetoService = objetoService;
	}
	
	@Override
	protected Service<Objeto> getService() {
		return this.objetoService;
	}

}
