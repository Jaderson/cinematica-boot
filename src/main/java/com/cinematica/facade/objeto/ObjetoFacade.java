package com.cinematica.facade.objeto;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.Objeto;

public interface ObjetoFacade extends Facade<Objeto>{

}
