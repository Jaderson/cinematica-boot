package com.cinematica.facade.acao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.acao.AcaoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Acao;
import com.cinematica.service.acao.AcaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AcaoFacadeImpl extends AbstractFacade<Acao> implements AcaoFacade{

	private AcaoService acaoService;

	@Autowired
	public AcaoFacadeImpl(AcaoService acaoService) {
		this.acaoService = acaoService;
	}
	
	@Override
	protected Service<Acao> getService() {
		return this.acaoService;
	}

}
