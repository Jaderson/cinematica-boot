package com.cinematica.facade.acao;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.Acao;

public interface AcaoFacade extends Facade<Acao>{

}
