package com.cinematica.facade.especialidade;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.especialidade.Especialidade;

public interface EspecialidadeFacade extends Facade<Especialidade>{

}
