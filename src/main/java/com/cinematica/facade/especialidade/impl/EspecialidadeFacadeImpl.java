package com.cinematica.facade.especialidade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.especialidade.EspecialidadeFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.service.especialidade.EspecialidadeService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class EspecialidadeFacadeImpl extends AbstractFacade<Especialidade> implements EspecialidadeFacade{

	private EspecialidadeService especialidadeService;

	@Autowired
	public EspecialidadeFacadeImpl(EspecialidadeService especialidadeService) {
		this.especialidadeService = especialidadeService;
	}
	
	@Override
	protected Service<Especialidade> getService() {
		return this.especialidadeService;
	}

}
