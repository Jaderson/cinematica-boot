package com.cinematica.facade.dataFalta;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.agenda.DataFalta;

public interface DataFaltaFacade extends Facade<DataFalta>{
}
