package com.cinematica.facade.dataFalta.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.dataFalta.DataFaltaFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.DataFalta;
import com.cinematica.service.dataFalta.DataFaltaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class DataFaltaFacadeImpl extends AbstractFacade<DataFalta> implements DataFaltaFacade{

	private DataFaltaService dataFaltaService;

	@Autowired
	public DataFaltaFacadeImpl(DataFaltaService dataFaltaService) {
		this.dataFaltaService = dataFaltaService;
	}
	
	@Override
	protected Service<DataFalta> getService() {
		return this.dataFaltaService;
	}

}
