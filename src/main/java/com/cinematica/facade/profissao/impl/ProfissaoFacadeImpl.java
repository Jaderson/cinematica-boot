package com.cinematica.facade.profissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.profissao.ProfissaoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.profissao.Profissao;
import com.cinematica.service.profissao.ProfissaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class ProfissaoFacadeImpl extends AbstractFacade<Profissao> implements ProfissaoFacade{

	private ProfissaoService profissoesService;

	@Autowired
	public ProfissaoFacadeImpl(ProfissaoService profissoesService) {
		this.profissoesService = profissoesService;
	}
	
	@Override
	protected Service<Profissao> getService() {
		return this.profissoesService;
	}

}
