package com.cinematica.facade.profissao;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.profissao.Profissao;

public interface ProfissaoFacade extends Facade<Profissao>{

}
