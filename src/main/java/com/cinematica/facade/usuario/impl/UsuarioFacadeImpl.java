package com.cinematica.facade.usuario.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.usuario.UsuarioFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.service.usuario.UsuarioService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioFacadeImpl extends AbstractFacade<Usuario> implements UsuarioFacade{

	private UsuarioService usuarioService;

	@Autowired
	public UsuarioFacadeImpl(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	protected Service<Usuario> getService() {
		return this.usuarioService;
	}

}
