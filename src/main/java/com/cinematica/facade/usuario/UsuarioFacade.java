package com.cinematica.facade.usuario;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.Usuario;

public interface UsuarioFacade extends Facade<Usuario>{

}
