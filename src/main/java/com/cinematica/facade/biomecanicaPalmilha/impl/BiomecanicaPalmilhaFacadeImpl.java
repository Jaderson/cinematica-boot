package com.cinematica.facade.biomecanicaPalmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.biomecanicaPalmilha.BiomecanicaPalmilhaFacade;
import com.cinematica.framework.avaliacoes.BiomecanicaPalmilha;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.service.biomecanicaPalmilha.BiomecanicaPalmilhaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class BiomecanicaPalmilhaFacadeImpl extends AbstractFacade<BiomecanicaPalmilha> implements BiomecanicaPalmilhaFacade{

	private BiomecanicaPalmilhaService biomecanicaPalmilhaService;

	@Autowired
	public BiomecanicaPalmilhaFacadeImpl(BiomecanicaPalmilhaService biomecanicaPalmilhaService) {
		this.biomecanicaPalmilhaService = biomecanicaPalmilhaService;
	}
	
	@Override
	protected Service<BiomecanicaPalmilha> getService() {
		return this.biomecanicaPalmilhaService;
	}

}
