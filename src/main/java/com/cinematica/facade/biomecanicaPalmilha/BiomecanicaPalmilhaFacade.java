package com.cinematica.facade.biomecanicaPalmilha;

import com.cinematica.framework.avaliacoes.BiomecanicaPalmilha;
import com.cinematica.framework.facade.Facade;

public interface BiomecanicaPalmilhaFacade extends Facade<BiomecanicaPalmilha>{

}
