package com.cinematica.facade.formaPagamento;

import com.cinematica.framework.facade.Facade;
import com.cinematica.framework.formaPagamento.FormaPagamento;

public interface FormaPagamentoFacade extends Facade<FormaPagamento>{

}
