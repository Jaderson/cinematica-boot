package com.cinematica.facade.formaPagamento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.formaPagamento.FormaPagamentoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.formaPagamento.FormaPagamento;
import com.cinematica.framework.service.Service;
import com.cinematica.service.formaPagamento.FormaPagamentoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class FormaPagamentoFacadeImpl extends AbstractFacade<FormaPagamento> implements FormaPagamentoFacade{

	private FormaPagamentoService formaPagamentoService;

	@Autowired
	public FormaPagamentoFacadeImpl(FormaPagamentoService formaPagamentoService) {
		this.formaPagamentoService = formaPagamentoService;
	}
	
	@Override
	protected Service<FormaPagamento> getService() {
		return this.formaPagamentoService;
	}

}
