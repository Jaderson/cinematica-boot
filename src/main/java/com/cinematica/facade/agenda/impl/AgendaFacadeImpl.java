package com.cinematica.facade.agenda.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.agenda.AgendaFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.service.agenda.AgendaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AgendaFacadeImpl extends AbstractFacade<Agenda> implements AgendaFacade{

	private AgendaService agendaService;

	@Autowired
	public AgendaFacadeImpl(AgendaService agendaService) {
		this.agendaService = agendaService;
	}
	
	@Override
	protected Service<Agenda> getService() {
		return this.agendaService;
	}

	@Override
	public List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente) {
		return agendaService.consultarFaltaMes(pegarPrimerioDataMesCorrente,pegarUltimaDataMesCorrente);
	}

	@Override
	public List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa) {
		return agendaService.consultaTipoEspecialidade(pegarPrimerioDataMesCorrente,pegarUltimaDataMesCorrente,idEmpresa);
	}

}
