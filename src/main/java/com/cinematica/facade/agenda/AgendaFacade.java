package com.cinematica.facade.agenda;

import java.util.Date;
import java.util.List;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.agenda.Agenda;

public interface AgendaFacade extends Facade<Agenda>{

	List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente);

	List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresass);

}
