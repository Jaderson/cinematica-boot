package com.cinematica.facade.perfil;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.Perfil;

public interface PerfilFacade extends Facade<Perfil>{

}
