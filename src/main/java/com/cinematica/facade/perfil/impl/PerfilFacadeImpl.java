package com.cinematica.facade.perfil.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.perfil.PerfilFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.service.perfil.PerfilService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PerfilFacadeImpl extends AbstractFacade<Perfil> implements PerfilFacade{

	private PerfilService perfilService;

	@Autowired
	public PerfilFacadeImpl(PerfilService perfilService) {
		this.perfilService = perfilService;
	}
	
	@Override
	protected Service<Perfil> getService() {
		return this.perfilService;
	}

}
