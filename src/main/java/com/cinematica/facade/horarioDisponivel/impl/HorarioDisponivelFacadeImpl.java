package com.cinematica.facade.horarioDisponivel.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.horarioDisponivel.HorarioDisponivelFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.service.horarioDisponivel.HorarioDisponivelService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class HorarioDisponivelFacadeImpl extends AbstractFacade<HorarioDisponivel> implements HorarioDisponivelFacade{

	private HorarioDisponivelService horarioDisponivelService;

	@Autowired
	public HorarioDisponivelFacadeImpl(HorarioDisponivelService horarioDisponivelService) {
		this.horarioDisponivelService = horarioDisponivelService;
	}
	
	@Override
	protected Service<HorarioDisponivel> getService() {
		return this.horarioDisponivelService;
	}

}
