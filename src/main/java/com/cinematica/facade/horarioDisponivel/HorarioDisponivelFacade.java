package com.cinematica.facade.horarioDisponivel;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.agenda.HorarioDisponivel;

public interface HorarioDisponivelFacade extends Facade<HorarioDisponivel>{

}
