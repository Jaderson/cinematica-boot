package com.cinematica.facade.configuracao;

import java.util.List;

import com.cinematica.framework.configuracao.Configuracao;
import com.cinematica.framework.facade.Facade;

public interface ConfiguracaoFacade extends Facade<Configuracao>{

	List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int verificarDiaSemanaPorData, Integer idEmpresa);

}
