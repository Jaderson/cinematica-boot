package com.cinematica.facade.contasPagar.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.contasPagar.ContasPagarFacade;
import com.cinematica.framework.contasPagar.ContasPagar;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.service.contasPagar.ContasPagarService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class ContasPagarFacadeImpl extends AbstractFacade<ContasPagar> implements ContasPagarFacade{

	private ContasPagarService contasPagarService;

	@Autowired
	public ContasPagarFacadeImpl(ContasPagarService contasPagarService) {
		this.contasPagarService = contasPagarService;
	}
	
	@Override
	protected Service<ContasPagar> getService() {
		return this.contasPagarService;
	}

	@Override
	public void excluirContasParceladas(ContasPagar entidade) {
		this.contasPagarService.excluirContasParceladas(entidade);
	}

}
