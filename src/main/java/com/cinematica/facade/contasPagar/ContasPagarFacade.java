package com.cinematica.facade.contasPagar;

import com.cinematica.framework.contasPagar.ContasPagar;
import com.cinematica.framework.facade.Facade;

public interface ContasPagarFacade extends Facade<ContasPagar>{

	void excluirContasParceladas(ContasPagar entidade);

}
