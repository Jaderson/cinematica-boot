package com.cinematica.facade.permissao;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.Permissao;

public interface PermissaoFacade extends Facade<Permissao>{

}
