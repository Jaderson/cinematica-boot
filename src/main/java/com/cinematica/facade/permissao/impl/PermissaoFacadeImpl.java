package com.cinematica.facade.permissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.permissao.PermissaoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.service.permissao.PermissaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PermissaoFacadeImpl extends AbstractFacade<Permissao> implements PermissaoFacade{

	private PermissaoService permissaoService;

	@Autowired
	public PermissaoFacadeImpl(PermissaoService permissaoService) {
		this.permissaoService = permissaoService;
	}
	
	@Override
	protected Service<Permissao> getService() {
		return this.permissaoService;
	}

}
