package com.cinematica.facade.pessoa;

import java.util.List;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.pessoa.Pessoa;

public interface PessoaFacade extends Facade<Pessoa>{

	List<Pessoa> consultarAniversariante();
}
