package com.cinematica.facade.pessoa.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.pessoa.PessoaFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.pessoa.Pessoa;
import com.cinematica.service.pessoa.PessoaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PessoaFacadeImpl extends AbstractFacade<Pessoa> implements PessoaFacade{

	private PessoaService pessoaService;

	@Autowired
	public PessoaFacadeImpl(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	
	@Override
	protected Service<Pessoa> getService() {
		return this.pessoaService;
	}

	@Override
	public List<Pessoa> consultarAniversariante() {
		return this.pessoaService.consultarAniversariante();
	}
}
