package com.cinematica.facade.categoria.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.categoria.CategoriaFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.framework.tipoCategoria.Categoria;
import com.cinematica.service.categoria.CategoriaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class CategoriaFacadeImpl extends AbstractFacade<Categoria> implements CategoriaFacade{

	private CategoriaService categoriaService;

	@Autowired
	public CategoriaFacadeImpl(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	protected Service<Categoria> getService() {
		return this.categoriaService;
	}

}
