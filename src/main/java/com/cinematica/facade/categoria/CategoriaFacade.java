package com.cinematica.facade.categoria;

import com.cinematica.framework.facade.Facade;
import com.cinematica.framework.tipoCategoria.Categoria;

public interface CategoriaFacade extends Facade<Categoria>{

}
