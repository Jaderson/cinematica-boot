package com.cinematica.facade.perfilPermissao;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Permissao;

public interface PerfilPermissaoFacade extends Facade<PerfilPermissao>{

	PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao);
}
