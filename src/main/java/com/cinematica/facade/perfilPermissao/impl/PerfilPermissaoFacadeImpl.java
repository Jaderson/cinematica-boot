package com.cinematica.facade.perfilPermissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.perfilPermissao.PerfilPermissaoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.service.perfilPermissao.PerfilPermissaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PerfilPermissaoFacadeImpl extends AbstractFacade<PerfilPermissao> implements PerfilPermissaoFacade{

	private PerfilPermissaoService perfilPermissaoService;

	@Autowired
	public PerfilPermissaoFacadeImpl(PerfilPermissaoService perfilPermissaoService) {
		this.perfilPermissaoService = perfilPermissaoService;
	}
	
	@Override
	public PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao) {
		return this.perfilPermissaoService.consultarPerfilPermissaoPorPerfilIhPermissao(perfil, permissao);
	}
	
	@Override
	protected Service<PerfilPermissao> getService() {
		return this.perfilPermissaoService;
	}
}
