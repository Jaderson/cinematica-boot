package com.cinematica.facade.palmilha;

import com.cinematica.framework.avaliacoes.Palmilha;
import com.cinematica.framework.facade.Facade;

public interface PalmilhaFacade extends Facade<Palmilha>{

}
