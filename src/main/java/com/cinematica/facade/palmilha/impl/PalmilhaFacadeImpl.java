package com.cinematica.facade.palmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.palmilha.PalmilhaFacade;
import com.cinematica.framework.avaliacoes.Palmilha;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.service.palmilha.PalmilhaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PalmilhaFacadeImpl extends AbstractFacade<Palmilha> implements PalmilhaFacade{

	private PalmilhaService palmilhaService;

	@Autowired
	public PalmilhaFacadeImpl(PalmilhaService palmilhaService) {
		this.palmilhaService = palmilhaService;
	}
	
	@Override
	protected Service<Palmilha> getService() {
		return this.palmilhaService;
	}

}
