package com.cinematica.facade.empresa;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.empresa.Empresa;

public interface EmpresaFacade extends Facade<Empresa>{

}
