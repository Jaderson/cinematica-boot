package com.cinematica.facade.empresa.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.empresa.EmpresaFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.empresa.Empresa;
import com.cinematica.service.empresa.EmpresaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class EmpresaFacadeImpl extends AbstractFacade<Empresa> implements EmpresaFacade{

	private EmpresaService empresaService;

	@Autowired
	public EmpresaFacadeImpl(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected Service<Empresa> getService() {
		return this.empresaService;
	}

}
