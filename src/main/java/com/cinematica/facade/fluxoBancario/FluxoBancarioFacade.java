package com.cinematica.facade.fluxoBancario;

import com.cinematica.framework.facade.Facade;
import com.cinematica.framework.fluxoBancario.FluxoBancario;

public interface FluxoBancarioFacade extends Facade<FluxoBancario>{

}
