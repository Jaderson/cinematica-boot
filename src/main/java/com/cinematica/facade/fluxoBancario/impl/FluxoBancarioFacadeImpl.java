package com.cinematica.facade.fluxoBancario.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.fluxoBancario.FluxoBancarioFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.fluxoBancario.FluxoBancario;
import com.cinematica.framework.service.Service;
import com.cinematica.service.fluxoBancario.FluxoBancarioService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class FluxoBancarioFacadeImpl extends AbstractFacade<FluxoBancario> implements FluxoBancarioFacade{

	private FluxoBancarioService fluxoBancarioService;

	@Autowired
	public FluxoBancarioFacadeImpl(FluxoBancarioService fluxoBancarioService) {
		this.fluxoBancarioService = fluxoBancarioService;
	}
	
	@Override
	protected Service<FluxoBancario> getService() {
		return this.fluxoBancarioService;
	}

}
