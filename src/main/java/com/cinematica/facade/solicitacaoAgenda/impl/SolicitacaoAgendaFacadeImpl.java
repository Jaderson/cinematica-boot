package com.cinematica.facade.solicitacaoAgenda.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.solicitacaoAgenda.SolicitacaoAgendaFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.cinematica.service.solicitacaoAgenda.SolicitacaoAgendaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class SolicitacaoAgendaFacadeImpl extends AbstractFacade<SolicitacaoAgenda> implements SolicitacaoAgendaFacade{

	private SolicitacaoAgendaService solicitacaoAgendaService;

	@Autowired
	public SolicitacaoAgendaFacadeImpl(SolicitacaoAgendaService solicitacaoAgendaService) {
		this.solicitacaoAgendaService = solicitacaoAgendaService;
	}
	
	@Override
	protected Service<SolicitacaoAgenda> getService() {
		return this.solicitacaoAgendaService;
	}

}
