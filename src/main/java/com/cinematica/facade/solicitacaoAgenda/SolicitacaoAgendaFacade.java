package com.cinematica.facade.solicitacaoAgenda;

import com.cinematica.framework.facade.Facade;
import com.cinematica.framework.solicitacaoAgenda.SolicitacaoAgenda;

public interface SolicitacaoAgendaFacade extends Facade<SolicitacaoAgenda>{

}
