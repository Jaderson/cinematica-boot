package com.cinematica.facade.medico.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.medico.MedicoFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.medico.Medico;
import com.cinematica.service.medico.MedicoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class MedicoFacadeImpl extends AbstractFacade<Medico> implements MedicoFacade{

	private MedicoService medicoService;

	@Autowired
	public MedicoFacadeImpl(MedicoService medicoService) {
		this.medicoService = medicoService;
	}
	
	@Override
	protected Service<Medico> getService() {
		return this.medicoService;
	}

}
