package com.cinematica.facade.medico;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.medico.Medico;

public interface MedicoFacade extends Facade<Medico>{

}
