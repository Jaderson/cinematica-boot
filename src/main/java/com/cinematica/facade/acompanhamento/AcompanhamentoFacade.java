package com.cinematica.facade.acompanhamento;

import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.framework.facade.Facade;

public interface AcompanhamentoFacade extends Facade<AcompanhamentoPessoa>{

}
