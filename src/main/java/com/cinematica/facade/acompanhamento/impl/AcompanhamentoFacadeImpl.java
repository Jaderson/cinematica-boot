package com.cinematica.facade.acompanhamento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.acompanhamento.AcompanhamentoFacade;
import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.service.acompanhamento.AcompanhamentoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AcompanhamentoFacadeImpl extends AbstractFacade<AcompanhamentoPessoa> implements AcompanhamentoFacade{

	private AcompanhamentoService acompanhamentoService;

	@Autowired
	public AcompanhamentoFacadeImpl(AcompanhamentoService acompanhamentoService) {
		this.acompanhamentoService = acompanhamentoService;
	}
	
	@Override
	protected Service<AcompanhamentoPessoa> getService() {
		return this.acompanhamentoService;
	}

}
