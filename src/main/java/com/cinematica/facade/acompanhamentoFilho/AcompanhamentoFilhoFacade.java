package com.cinematica.facade.acompanhamentoFilho;

import com.cinematica.framework.acompanhamento.AcompanhamentoFilho;
import com.cinematica.framework.facade.Facade;

public interface AcompanhamentoFilhoFacade extends Facade<AcompanhamentoFilho>{

}
