package com.cinematica.facade.fluxoCaixa.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.fluxoCaixa.FluxoCaixaFacade;
import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.caixa.TipoLancamento;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.service.fluxoCaixa.FluxoCaixaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class FluxoCaixaFacadeImpl extends AbstractFacade<FluxoCaixa> implements FluxoCaixaFacade{

	private FluxoCaixaService fluxoCaixaService;

	@Autowired
	public FluxoCaixaFacadeImpl(FluxoCaixaService fluxoCaixaService) {
		this.fluxoCaixaService = fluxoCaixaService;
	}
	
	@Override
	protected Service<FluxoCaixa> getService() {
		return this.fluxoCaixaService;
	}

	@Override
	public BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia, Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento) {
		return this.fluxoCaixaService.consultarDataLancamento(geraDataComPrimeiroDia,geraDataComUltimoDia, idEmpresa, tipoLancamento);
	}

}
