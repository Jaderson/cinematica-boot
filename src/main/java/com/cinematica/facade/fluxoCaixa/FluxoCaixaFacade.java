package com.cinematica.facade.fluxoCaixa;

import java.math.BigDecimal;
import java.util.Date;

import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.caixa.TipoLancamento;
import com.cinematica.framework.facade.Facade;

public interface FluxoCaixaFacade extends Facade<FluxoCaixa>{

	BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia,Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento);

}
