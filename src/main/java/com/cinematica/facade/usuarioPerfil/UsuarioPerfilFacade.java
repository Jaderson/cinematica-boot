package com.cinematica.facade.usuarioPerfil;

import com.cinematica.framework.facade.Facade;
import com.cinematica.model.autenticacao.UsuarioPerfil;

public interface UsuarioPerfilFacade extends Facade<UsuarioPerfil>{

}
