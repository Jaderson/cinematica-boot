package com.cinematica.facade.usuarioPerfil.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cinematica.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.cinematica.framework.facade.impl.AbstractFacade;
import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.UsuarioPerfil;
import com.cinematica.service.usuarioPerfil.UsuarioPerfilService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioPerfilFacadeImpl extends AbstractFacade<UsuarioPerfil> implements UsuarioPerfilFacade{

	private UsuarioPerfilService usuarioPerfilService;

	@Autowired
	public UsuarioPerfilFacadeImpl(UsuarioPerfilService usuarioPerfilService) {
		this.usuarioPerfilService = usuarioPerfilService;
	}
	
	@Override
	protected Service<UsuarioPerfil> getService() {
		return this.usuarioPerfilService;
	}

}
