package com.cinematica.service.permissao;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Permissao;

public interface PermissaoService extends Service<Permissao>{
}
