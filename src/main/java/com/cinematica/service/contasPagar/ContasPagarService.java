package com.cinematica.service.contasPagar;

import com.cinematica.framework.contasPagar.ContasPagar;
import com.cinematica.framework.service.Service;

public interface ContasPagarService extends Service<ContasPagar>{

	void verificarCamposObrigatorios(ContasPagar entidade);

	void excluirContasParceladas(ContasPagar entidade);
}
