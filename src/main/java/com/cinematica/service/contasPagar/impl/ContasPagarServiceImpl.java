package com.cinematica.service.contasPagar.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.contasPagar.ContasPagar;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.contasPagar.ContasPagarRepository;
import com.cinematica.service.contasPagar.ContasPagarService;

@Component
public class ContasPagarServiceImpl extends AbstractService<ContasPagar> implements ContasPagarService{

	private ContasPagarRepository contasPagarRepository;

	@Autowired
	public ContasPagarServiceImpl(Validador validador, ContasPagarRepository contasPagarRepository) {
		super(validador);
		this.contasPagarRepository = contasPagarRepository;
	}

	@Override
	protected Repository<ContasPagar> getRepository() {
		return this.contasPagarRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(ContasPagar entidade) {
		verificarSeContaEParceladas(entidade);
	}
	
	private void verificarSeContaEParceladas(ContasPagar entidade) {
		if(!entidade.isContaFixa() && entidade.isPago() != true){
			throw new ValidationException("nao_e_permitido_alterar_contas_parceladas");
		}
	}

	@Override
	public void verificarCamposObrigatorios(ContasPagar entidade) {
			
			StringBuffer msg = new StringBuffer();
			msg.append(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula;"));
			/*msg.append(verificarSeCampoEstaNulo(entidade.getCategoria(), "erro_categoria_nao_pode_ser_nula;"));*/
			msg.append(verificarSeCampoEstaNulo(entidade.getValor(), "erro_valor_nao_pode_ser_nula;"));
			if (msg.length() > 0) {
				throw new ValidationException(msg.toString());
			}
	}

	@Override
	public void excluirContasParceladas(ContasPagar entidade) {
		contasPagarRepository.alterar(entidade);
	}
	
}
