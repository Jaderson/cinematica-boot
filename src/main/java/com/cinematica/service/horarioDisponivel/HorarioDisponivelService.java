package com.cinematica.service.horarioDisponivel;

import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.HorarioDisponivel;

public interface HorarioDisponivelService extends Service<HorarioDisponivel>{
}
