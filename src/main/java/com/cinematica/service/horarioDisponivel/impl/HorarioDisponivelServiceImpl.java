package com.cinematica.service.horarioDisponivel.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.repository.Repository;
import com.cinematica.repository.horarioDisponivel.HorarioDisponivelRepository;
import com.cinematica.service.horarioDisponivel.HorarioDisponivelService;

@Component
public class HorarioDisponivelServiceImpl extends AbstractService<HorarioDisponivel> implements HorarioDisponivelService{

	private HorarioDisponivelRepository horarioDisponivelRepository;

	@Autowired
	public HorarioDisponivelServiceImpl(Validador validador, HorarioDisponivelRepository horarioDisponivelRepository) {
		super(validador);
		this.horarioDisponivelRepository = horarioDisponivelRepository;
	}

	@Override
	protected Repository<HorarioDisponivel> getRepository() {
		return this.horarioDisponivelRepository;
	}

}
