package com.cinematica.service.banco;

import com.cinematica.framework.banco.Banco;
import com.cinematica.framework.service.Service;

public interface BancoService extends Service<Banco>{
}
