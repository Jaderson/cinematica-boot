package com.cinematica.service.banco.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.banco.Banco;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.banco.BancoRepository;
import com.cinematica.service.banco.BancoService;

@Component
public class BancoServiceImpl extends AbstractService<Banco> implements BancoService{

	private BancoRepository bancoRepository;

	@Autowired
	public BancoServiceImpl(Validador validador, BancoRepository bancoRepository) {
		super(validador);
		this.bancoRepository = bancoRepository;
	}

	@Override
	protected Repository<Banco> getRepository() {
		return this.bancoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Banco entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Banco entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Banco entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_nome_não_pode_ser_nulo").toString());
		}
	}
}
