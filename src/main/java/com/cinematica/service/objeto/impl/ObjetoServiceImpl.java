package com.cinematica.service.objeto.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.autenticacao.Objeto;
import com.cinematica.repository.Repository;
import com.cinematica.repository.objeto.ObjetoRepository;
import com.cinematica.service.objeto.ObjetoService;

@Component
public class ObjetoServiceImpl extends AbstractService<Objeto> implements ObjetoService{

	private ObjetoRepository objetoRepository;

	@Autowired
	public ObjetoServiceImpl(Validador validador, ObjetoRepository objetoRepository) {
		super(validador);
		this.objetoRepository = objetoRepository;
	}

	@Override
	protected Repository<Objeto> getRepository() {
		return this.objetoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Objeto entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Objeto entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Objeto entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
