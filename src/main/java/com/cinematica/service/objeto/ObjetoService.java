package com.cinematica.service.objeto;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Objeto;

public interface ObjetoService extends Service<Objeto>{
}
