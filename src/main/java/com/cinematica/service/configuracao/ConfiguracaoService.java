package com.cinematica.service.configuracao;

import java.util.List;

import com.cinematica.framework.configuracao.Configuracao;
import com.cinematica.framework.service.Service;

public interface ConfiguracaoService extends Service<Configuracao>{

	List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int idSemana, Integer idEmpresa);

}
