package com.cinematica.service.empresa;

import com.cinematica.framework.service.Service;
import com.cinematica.model.empresa.Empresa;

public interface EmpresaService extends Service<Empresa>{
}
