package com.cinematica.service.palmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.Palmilha;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.palmilha.PalmilhaRepository;
import com.cinematica.service.palmilha.PalmilhaService;

@Component
public class PalmilhaServiceImpl extends AbstractService<Palmilha> implements PalmilhaService{

	private PalmilhaRepository palmilhaRepository;

	@Autowired
	public PalmilhaServiceImpl(Validador validador, PalmilhaRepository palmilhaRepository) {
		super(validador);
		this.palmilhaRepository = palmilhaRepository;
	}

	@Override
	protected Repository<Palmilha> getRepository() {
		return this.palmilhaRepository;
	}

}
