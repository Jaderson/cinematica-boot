package com.cinematica.service.perfil;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Perfil;

public interface PerfilService extends Service<Perfil>{
}
