package com.cinematica.service.fisioterapia;

import com.cinematica.framework.avaliacoes.Fisioterapia;
import com.cinematica.framework.service.Service;

public interface FisioterapiaService extends Service<Fisioterapia>{
}
