package com.cinematica.service.fisioterapia.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.Fisioterapia;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.fisioterapia.FisioterapiaRepository;
import com.cinematica.service.fisioterapia.FisioterapiaService;

@Component
public class FisioterapiaServiceImpl extends AbstractService<Fisioterapia> implements FisioterapiaService{

	private FisioterapiaRepository fisioterapiaRepository;

	@Autowired
	public FisioterapiaServiceImpl(Validador validador, FisioterapiaRepository fisioterapiaRepository) {
		super(validador);
		this.fisioterapiaRepository = fisioterapiaRepository;
	}

	@Override
	protected Repository<Fisioterapia> getRepository() {
		return this.fisioterapiaRepository;
	}

}
