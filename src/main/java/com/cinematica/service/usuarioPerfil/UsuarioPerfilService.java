package com.cinematica.service.usuarioPerfil;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.UsuarioPerfil;

public interface UsuarioPerfilService extends Service<UsuarioPerfil>{
}
