package com.cinematica.service.usuarioPerfil.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.autenticacao.UsuarioPerfil;
import com.cinematica.repository.Repository;
import com.cinematica.repository.usuarioPerfil.UsuarioPerfilRepository;
import com.cinematica.service.usuarioPerfil.UsuarioPerfilService;

@Component
public class UsuarioPerfilServiceImpl extends AbstractService<UsuarioPerfil> implements UsuarioPerfilService{

	private UsuarioPerfilRepository usuarioPerfilRepository;

	@Autowired
	public UsuarioPerfilServiceImpl(Validador validador, UsuarioPerfilRepository usuarioPerfilRepository) {
		super(validador);
		this.usuarioPerfilRepository = usuarioPerfilRepository;
	}

	@Override
	protected Repository<UsuarioPerfil> getRepository() {
		return this.usuarioPerfilRepository;
	}

}
