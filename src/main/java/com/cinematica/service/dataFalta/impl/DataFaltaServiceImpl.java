package com.cinematica.service.dataFalta.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.agenda.DataFalta;
import com.cinematica.repository.Repository;
import com.cinematica.repository.dataFalta.DataFaltaRepository;
import com.cinematica.service.dataFalta.DataFaltaService;

@Component
public class DataFaltaServiceImpl extends AbstractService<DataFalta> implements DataFaltaService{

	private DataFaltaRepository dataFaltaRepository;

	@Autowired
	public DataFaltaServiceImpl(Validador validador, DataFaltaRepository dataFaltaRepository) {
		super(validador);
		this.dataFaltaRepository = dataFaltaRepository;
	}

	@Override
	protected Repository<DataFalta> getRepository() {
		return this.dataFaltaRepository;
	}

}
