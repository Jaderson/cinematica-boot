package com.cinematica.service.categoria;

import com.cinematica.framework.service.Service;
import com.cinematica.framework.tipoCategoria.Categoria;

public interface CategoriaService extends Service<Categoria>{
}
