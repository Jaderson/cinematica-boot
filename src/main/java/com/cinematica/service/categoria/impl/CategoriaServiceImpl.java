package com.cinematica.service.categoria.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.tipoCategoria.Categoria;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.categoria.CategoriaRepository;
import com.cinematica.service.categoria.CategoriaService;

@Component
public class CategoriaServiceImpl extends AbstractService<Categoria> implements CategoriaService{

	private CategoriaRepository categoriaRepository;

	@Autowired
	public CategoriaServiceImpl(Validador validador, CategoriaRepository categoriaRepository) {
		super(validador);
		this.categoriaRepository = categoriaRepository;
	}

	@Override
	protected Repository<Categoria> getRepository() {
		return this.categoriaRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Categoria entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Categoria entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Categoria entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
