package com.cinematica.service.fluxoCaixa.impl;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.caixa.TipoLancamento;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.fluxoCaixa.FluxoCaixaRepository;
import com.cinematica.service.fluxoCaixa.FluxoCaixaService;

@Component
public class FluxoCaixaServiceImpl extends AbstractService<FluxoCaixa> implements FluxoCaixaService{

	private FluxoCaixaRepository fluxoCaixaRepository;

	@Autowired
	public FluxoCaixaServiceImpl(Validador validador, FluxoCaixaRepository fluxoCaixaRepository) {
		super(validador);
		this.fluxoCaixaRepository = fluxoCaixaRepository;
	}

	@Override
	protected Repository<FluxoCaixa> getRepository() {
		return this.fluxoCaixaRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(FluxoCaixa entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(FluxoCaixa entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(FluxoCaixa entidade) {
		
		StringBuffer msg = new StringBuffer();
		msg.append(verificarSeCampoEstaNulo(entidade.getValor(), "erro_valor_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getTipoLancamento(), "erro_voce_deve_escolher_entre_receita_ou_despesas_o_campo_nao_pode_ser_nulo;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getFormaPagamento(), "erro_forma_de_pagamento_nao_pode_ser_nula;"));
		
		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}

	@Override
	public BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia, Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento) {
		return this.fluxoCaixaRepository.consultarDataLancamento(geraDataComPrimeiroDia, geraDataComUltimoDia, idEmpresa, tipoLancamento);
	}

}
