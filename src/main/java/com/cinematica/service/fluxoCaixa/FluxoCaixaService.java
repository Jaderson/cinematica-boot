package com.cinematica.service.fluxoCaixa;

import java.math.BigDecimal;
import java.util.Date;

import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.caixa.TipoLancamento;
import com.cinematica.framework.service.Service;

public interface FluxoCaixaService extends Service<FluxoCaixa>{

	BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia,Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento);
}
