package com.cinematica.service.acompanhamento;

import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.framework.service.Service;

public interface AcompanhamentoService extends Service<AcompanhamentoPessoa>{
}
