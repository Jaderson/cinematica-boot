package com.cinematica.service.acompanhamento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.acompanhamento.AcompanhamentoRepository;
import com.cinematica.service.acompanhamento.AcompanhamentoService;

@Component
public class AcompanhamentoServiceImpl extends AbstractService<AcompanhamentoPessoa> implements AcompanhamentoService{

	private AcompanhamentoRepository acompanhamentoRepository;

	@Autowired
	public AcompanhamentoServiceImpl(Validador validador, AcompanhamentoRepository acompanhamentoRepository) {
		super(validador);
		this.acompanhamentoRepository = acompanhamentoRepository;
	}

	@Override
	protected Repository<AcompanhamentoPessoa> getRepository() {
		return this.acompanhamentoRepository;
	}
	
	@Override
	protected void regrasNegocioExcluir(AcompanhamentoPessoa entidade) {
		verificarSeExisteAcompanhamentoFilho(entidade);
	}

	private void verificarSeExisteAcompanhamentoFilho(AcompanhamentoPessoa entidade) {
		
	}

}
