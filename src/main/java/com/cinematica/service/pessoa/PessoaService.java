package com.cinematica.service.pessoa;

import java.util.List;

import com.cinematica.framework.service.Service;
import com.cinematica.model.pessoa.Pessoa;

public interface PessoaService extends Service<Pessoa>{

	List<Pessoa> consultarAniversariante();
}
