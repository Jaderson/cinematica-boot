package com.cinematica.service.pessoa.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.consulta.restricao.Restricoes;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.model.pessoa.Pessoa;
import com.cinematica.repository.Repository;
import com.cinematica.repository.acompanhamento.AcompanhamentoRepository;
import com.cinematica.repository.agenda.AgendaRepository;
import com.cinematica.repository.fluxoCaixa.FluxoCaixaRepository;
import com.cinematica.repository.pessoa.PessoaRepository;
import com.cinematica.repository.usuario.UsuarioRepository;
import com.cinematica.service.pessoa.PessoaService;

@Component
public class PessoaServiceImpl extends AbstractService<Pessoa> implements PessoaService{

	private PessoaRepository pessoaRepository;
	private UsuarioRepository usuarioRepository;
	private AcompanhamentoRepository acompanhamentoRepository;
	private FluxoCaixaRepository fluxoCaixaRepository;
	private AgendaRepository agendaRepository;

	@Autowired
	public PessoaServiceImpl(Validador validador, PessoaRepository pessoaRepository, UsuarioRepository usuarioRepository,
			AcompanhamentoRepository acompanhamentoRepository, FluxoCaixaRepository fluxoCaixaRepository) {
		super(validador);
		this.pessoaRepository = pessoaRepository;
		this.usuarioRepository = usuarioRepository;
		this.acompanhamentoRepository = acompanhamentoRepository;
		this.fluxoCaixaRepository = fluxoCaixaRepository;
	}

	@Override
	protected Repository<Pessoa> getRepository() {
		return this.pessoaRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Pessoa entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Pessoa entidade) {
		/*verificarCamposObrigatorios(entidade);*/
	}
	
	@Override
	protected void regrasNegocioExcluir(Pessoa entidade) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("pessoa.id", entidade.getId()));
		verificaSeExisteUsuario(listaRestricoes);
		verificarSeExisteEvolucao(listaRestricoes);
		verificarSeExisteFluxoCaixa(listaRestricoes);
		verificarSeExisteAgenda(listaRestricoes);
	}

	private void verificarSeExisteAgenda(List<Restricoes> listaRestricoes) {
		Agenda agenda = this.agendaRepository.consultarEntidade(new Agenda(), listaRestricoes);
		if (VerificadorUtil.naoEstaNulo(agenda)) {
			throw new RuntimeException("erro_impossivel_excluir_paciente_existe_agenda_marcada_para_esse_paciente");
		}
	}

	private void verificarSeExisteFluxoCaixa(List<Restricoes> listaRestricoes) {
		FluxoCaixa fluxoCaixa = this.fluxoCaixaRepository.consultarEntidade(new FluxoCaixa(), listaRestricoes);
		if (VerificadorUtil.naoEstaNulo(fluxoCaixa)) {
			throw new RuntimeException("erro_impossivel_excluir_paciente_existe_lancamentos_para_o_paciente");
		}
	}

	private void verificarSeExisteEvolucao(List<Restricoes> listaRestricoes) {
		AcompanhamentoPessoa acompanhamentoPessoa = this.acompanhamentoRepository.consultarEntidade(new AcompanhamentoPessoa(), listaRestricoes);
		if (VerificadorUtil.naoEstaNulo(acompanhamentoPessoa)) {
			throw new RuntimeException("erro_impossivel_excluir_paciente_existe_evolucao_para_o_paciente");
		}
	}

	private void verificaSeExisteUsuario(List<Restricoes> listaRestricoes) {
		Usuario usuario = this.usuarioRepository.consultarEntidade(new Usuario(), listaRestricoes);
		if (VerificadorUtil.naoEstaNulo(usuario)) {
			throw new RuntimeException("existem_usuarios_que_utilizam_este_paciente");
		}
	}
	
	public void verificarCamposObrigatorios(Pessoa entidade) {
		
		StringBuffer msg = new StringBuffer();
		/*msg.append(verificarSeCampoEstaNulo(entidade.getTipoPessoa(), "erro_tipo_pessoa_nao_pode_ser_nulo;"));*/
		msg.append(verificarSeCampoEstaNulo(entidade.getNome(), "erro_nome_não_pode_ser_nulo;"));
		/*msg.append(verificarSeCampoEstaNulo(entidade.getSexo(), "erro_voce_deve_escolher_entre_masculino_ou_feminino_o_campo_nao_pode_ser_nulo;"));*/
		msg.append(verificarSeCampoEstaNulo(entidade.getEmail(), "erro_o_email_nao_pode_ser_nulo;"));
		/*msg.append(verificarSeCampoEstaNulo(entidade.getProfissao(), "erro_profissao_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getDataNascimento(), "erro_data_nascimento_nao_pode_ser_nula;"));*/
		
		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}

	@Override
	public List<Pessoa> consultarAniversariante() {
		return this.pessoaRepository.consultarAniversariante();
	}
}
