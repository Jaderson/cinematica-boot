package com.cinematica.service.acao;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Acao;

public interface AcaoService extends Service<Acao>{
}
