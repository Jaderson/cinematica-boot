package com.cinematica.service.acao.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.autenticacao.Acao;
import com.cinematica.repository.Repository;
import com.cinematica.repository.acao.AcaoRepository;
import com.cinematica.service.acao.AcaoService;

@Component
public class AcaoServiceImpl extends AbstractService<Acao> implements AcaoService{

	private AcaoRepository acaoRepository;

	@Autowired
	public AcaoServiceImpl(Validador validador, AcaoRepository acaoRepository) {
		super(validador);
		this.acaoRepository = acaoRepository;
	}

	@Override
	protected Repository<Acao> getRepository() {
		return this.acaoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Acao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Acao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Acao entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
