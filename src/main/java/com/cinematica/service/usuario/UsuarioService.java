package com.cinematica.service.usuario;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Usuario;

public interface UsuarioService extends Service<Usuario>{

	Usuario login(String username, String senha);

	void verificaSeUsuarioEstaLogado(Usuario entidade);
}
