package com.cinematica.service.horario.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.consulta.restricao.Restricoes;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.agenda.Horario;
import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.repository.Repository;
import com.cinematica.repository.agenda.AgendaRepository;
import com.cinematica.repository.horario.HorarioRepository;
import com.cinematica.repository.horarioDisponivel.HorarioDisponivelRepository;
import com.cinematica.service.horario.HorarioService;

@Component
public class HorarioServiceImpl extends AbstractService<Horario> implements HorarioService{

	private HorarioRepository horarioRepository;
	private HorarioDisponivelRepository horarioDisponivelRepository;
	private AgendaRepository agendaRepository;

	@Autowired
	public HorarioServiceImpl(Validador validador, HorarioRepository horarioRepository, 
			HorarioDisponivelRepository horarioDisponivelRepository, AgendaRepository agendaRepository) {
		super(validador);
		this.horarioRepository = horarioRepository;
		this.horarioDisponivelRepository = horarioDisponivelRepository;
		this.agendaRepository = agendaRepository;
	}
	
	@Override
	protected void regrasNegocioExcluir(Horario entidade) {
		verificaSeExisteHorarioRelacionado(entidade);
		verificaSeExisteHorarioNaAgenda(entidade);
		super.regrasNegocioExcluir(entidade);
	}

	@Override
	protected Repository<Horario> getRepository() {
		return this.horarioRepository;
	}

	@Override
	public List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade) {
		return horarioRepository.consultarHorarioPorConfiguracao(entidade);
	}
	
	private void verificaSeExisteHorarioRelacionado(Horario entidade) {
		List<Restricoes> listaRestricoesHorario = new ArrayList<Restricoes>();
		listaRestricoesHorario.add(Restricoes.igual("horario.id", entidade.getId()));
		List<HorarioDisponivel> listHorarioDisponivel = this.horarioDisponivelRepository
				.consultarTodos(new HorarioDisponivel(), listaRestricoesHorario, null);	
		if(listHorarioDisponivel.size() > 0) {
			throw new ValidationException("impossivel_excluir_existem_consultas_que_utilizam_este_horario");
		}
	}

	private void verificaSeExisteHorarioNaAgenda(Horario entidade) {
		List<Restricoes> listaRestricoesHorario = new ArrayList<Restricoes>();
		listaRestricoesHorario.add(Restricoes.igual("horario.id", entidade.getId()));
		List<Agenda> listaAgenda = this.agendaRepository
				.consultarTodos(new Agenda(), listaRestricoesHorario, null);	
		if(listaAgenda.size() > 0) {
			throw new ValidationException("impossivel_excluir_usuario_existem_consultas_marcadas");
		}
	}
}
