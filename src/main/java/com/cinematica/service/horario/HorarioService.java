package com.cinematica.service.horario;

import java.util.List;

import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.agenda.Horario;
import com.cinematica.model.agenda.HorarioDisponivel;

public interface HorarioService extends Service<Horario>{

	List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade);
}
