package com.cinematica.service.fluxoBancario;

import com.cinematica.framework.fluxoBancario.FluxoBancario;
import com.cinematica.framework.service.Service;

public interface FluxoBancarioService extends Service<FluxoBancario>{
}
