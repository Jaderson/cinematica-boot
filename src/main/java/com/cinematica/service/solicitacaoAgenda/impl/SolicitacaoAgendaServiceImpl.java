package com.cinematica.service.solicitacaoAgenda.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.solicitacaoAgenda.SolicitacaoAgendaRepository;
import com.cinematica.service.solicitacaoAgenda.SolicitacaoAgendaService;

@Component
public class SolicitacaoAgendaServiceImpl extends AbstractService<SolicitacaoAgenda> implements SolicitacaoAgendaService{

	private SolicitacaoAgendaRepository solicitacaoAgendaRepository;

	@Autowired
	public SolicitacaoAgendaServiceImpl(Validador validador, SolicitacaoAgendaRepository solicitacaoAgendaRepository) {
		super(validador);
		this.solicitacaoAgendaRepository = solicitacaoAgendaRepository;
	}

	@Override
	protected Repository<SolicitacaoAgenda> getRepository() {
		return this.solicitacaoAgendaRepository;
	}

}
