package com.cinematica.service.solicitacaoAgenda;

import com.cinematica.framework.service.Service;
import com.cinematica.framework.solicitacaoAgenda.SolicitacaoAgenda;

public interface SolicitacaoAgendaService extends Service<SolicitacaoAgenda>{
}
