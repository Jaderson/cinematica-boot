package com.cinematica.service.especialidade;

import com.cinematica.framework.service.Service;
import com.cinematica.model.especialidade.Especialidade;

public interface EspecialidadeService extends Service<Especialidade>{
}
