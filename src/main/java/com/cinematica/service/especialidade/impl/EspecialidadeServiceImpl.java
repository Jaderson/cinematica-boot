package com.cinematica.service.especialidade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.exception.EspecialidadeException;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.util.VerificadorUtil;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.repository.Repository;
import com.cinematica.repository.agenda.AgendaRepository;
import com.cinematica.repository.especialidade.EspecialidadeRepository;
import com.cinematica.service.especialidade.EspecialidadeService;

@Component
public class EspecialidadeServiceImpl extends AbstractService<Especialidade> implements EspecialidadeService{

	private EspecialidadeRepository especialidadeRepository;
	private AgendaRepository agendaRepository;

	@Autowired
	public EspecialidadeServiceImpl(Validador validador, EspecialidadeRepository especialidadeRepository,
			AgendaRepository agendaRepository) {
		super(validador);
		this.especialidadeRepository = especialidadeRepository;
		this.agendaRepository = agendaRepository;
	}

	@Override
	protected Repository<Especialidade> getRepository() {
		return this.especialidadeRepository;
	}
	
	@Override
	protected void regrasNegocioExcluir(Especialidade entidade) {
		verificaAgendaSeExisteEspecialidade(entidade);
	}
	
	private void verificaAgendaSeExisteEspecialidade(Especialidade entidade) {
		this.agendaRepository.consultarSeExisteEspecialidade(entidade);
	}

	@Override
	protected void regrasNegocioCadastrar(Especialidade entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioAlterar(Especialidade entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Especialidade entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new EspecialidadeException("erro_descricao_nao_pode_ser_nula");
		}
	}

}
