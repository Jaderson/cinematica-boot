package com.cinematica.service.formaPagamento;

import com.cinematica.framework.formaPagamento.FormaPagamento;
import com.cinematica.framework.service.Service;

public interface FormaPagamentoService extends Service<FormaPagamento>{
}
