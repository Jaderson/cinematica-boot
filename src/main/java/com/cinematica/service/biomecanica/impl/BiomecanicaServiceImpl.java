package com.cinematica.service.biomecanica.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.Biomecanica;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.biomecanica.BiomecanicaRepository;
import com.cinematica.service.biomecanica.BiomecanicaService;

@Component
public class BiomecanicaServiceImpl extends AbstractService<Biomecanica> implements BiomecanicaService{

	private BiomecanicaRepository biomecanicaRepository;

	@Autowired
	public BiomecanicaServiceImpl(Validador validador, BiomecanicaRepository biomecanicaRepository) {
		super(validador);
		this.biomecanicaRepository = biomecanicaRepository;
	}

	@Override
	protected Repository<Biomecanica> getRepository() {
		return this.biomecanicaRepository;
	}

}
