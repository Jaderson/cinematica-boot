package com.cinematica.service.biomecanica;

import com.cinematica.framework.avaliacoes.Biomecanica;
import com.cinematica.framework.service.Service;

public interface BiomecanicaService extends Service<Biomecanica>{
}
