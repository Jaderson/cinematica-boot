package com.cinematica.service.agenda;

import java.util.Date;
import java.util.List;

import com.cinematica.framework.service.Service;
import com.cinematica.model.agenda.Agenda;

public interface AgendaService extends Service<Agenda>{

	void verificarCamposObrigatorios(Agenda entidade);

	List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente);

	List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa);
}
