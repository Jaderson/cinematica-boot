package com.cinematica.service.agenda.impl;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.repository.Repository;
import com.cinematica.repository.agenda.AgendaRepository;
import com.cinematica.service.agenda.AgendaService;

@Component
public class AgendaServiceImpl extends AbstractService<Agenda> implements AgendaService{

	private AgendaRepository agendaRepository;

	@Autowired
	public AgendaServiceImpl(Validador validador, AgendaRepository agendaRepository) {
		super(validador);
		this.agendaRepository = agendaRepository;
	}

	@Override
	protected Repository<Agenda> getRepository() {
		return this.agendaRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Agenda entidade) {
		verificarCamposObrigatorios(entidade);
		super.regrasNegocioAlterar(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Agenda entidade) {
		verificarCamposObrigatorios(entidade);
		super.regrasNegocioCadastrar(entidade);
	}

	@Override
	public void verificarCamposObrigatorios(Agenda entidade) {
		
		StringBuffer msg = new StringBuffer();
		msg.append(verificarSeCampoEstaNulo(entidade.getPessoa(), "erro_pessoa_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getFuncionario(), "erro_funcionario_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getEspecialidade(), "erro_especialidade_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getHorario(), "erro_horario_nao_pode_ser_nula;"));
		
		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}

	@Override
	public List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente) {
		return agendaRepository.consultarFaltaMes(pegarPrimerioDataMesCorrente,pegarUltimaDataMesCorrente);
	}

	@Override
	public List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa) {
		return agendaRepository.consultaTipoEspecialidade(pegarPrimerioDataMesCorrente,pegarUltimaDataMesCorrente, idEmpresa);
	}

}
