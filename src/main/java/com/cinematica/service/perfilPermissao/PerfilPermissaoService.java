package com.cinematica.service.perfilPermissao;

import com.cinematica.framework.service.Service;
import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Permissao;

public interface PerfilPermissaoService extends Service<PerfilPermissao> {
	
	PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(Perfil perfil,	Permissao permissao);
}
