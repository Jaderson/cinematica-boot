package com.cinematica.service.perfilPermissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.repository.Repository;
import com.cinematica.repository.perfilPermissao.PerfilPermissaoRepository;
import com.cinematica.service.perfilPermissao.PerfilPermissaoService;

@Component
public class PerfilPermissaoServiceImpl extends AbstractService<PerfilPermissao> implements PerfilPermissaoService{

	private PerfilPermissaoRepository perfilPermissaoRepository;

	@Autowired
	public PerfilPermissaoServiceImpl(Validador validador, PerfilPermissaoRepository perfilPermissaoRepository) {
		super(validador);
		this.perfilPermissaoRepository = perfilPermissaoRepository;
	}

	@Override
	public PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao) {
		return this.perfilPermissaoRepository.consultarPerfilPermissaoPorPerfilIhPermissao(perfil, permissao);
	}
	
	@Override
	protected Repository<PerfilPermissao> getRepository() {
		return this.perfilPermissaoRepository;
	}

}
