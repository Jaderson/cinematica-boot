package com.cinematica.service.medico.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.model.medico.Medico;
import com.cinematica.repository.Repository;
import com.cinematica.repository.medico.MedicoRepository;
import com.cinematica.service.medico.MedicoService;

@Component
public class MedicoServiceImpl extends AbstractService<Medico> implements MedicoService{

	private MedicoRepository medicoRepository;

	@Autowired
	public MedicoServiceImpl(Validador validador, MedicoRepository medicoRepository) {
		super(validador);
		this.medicoRepository = medicoRepository;
	}

	@Override
	protected Repository<Medico> getRepository() {
		return this.medicoRepository;
	}

}
