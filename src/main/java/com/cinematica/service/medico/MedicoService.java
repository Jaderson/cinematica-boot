package com.cinematica.service.medico;

import com.cinematica.framework.service.Service;
import com.cinematica.model.medico.Medico;

public interface MedicoService extends Service<Medico>{
}
