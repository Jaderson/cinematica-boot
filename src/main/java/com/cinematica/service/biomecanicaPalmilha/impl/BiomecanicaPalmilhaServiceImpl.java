package com.cinematica.service.biomecanicaPalmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.BiomecanicaPalmilha;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.biomecanicaPalmilha.BiomecanicaPalmilhaRepository;
import com.cinematica.service.biomecanicaPalmilha.BiomecanicaPalmilhaService;

@Component
public class BiomecanicaPalmilhaServiceImpl extends AbstractService<BiomecanicaPalmilha> implements BiomecanicaPalmilhaService{

	private BiomecanicaPalmilhaRepository biomecanicaPalmilhaRepository;

	@Autowired
	public BiomecanicaPalmilhaServiceImpl(Validador validador, BiomecanicaPalmilhaRepository biomecanicaPalmilhaRepository) {
		super(validador);
		this.biomecanicaPalmilhaRepository = biomecanicaPalmilhaRepository;
	}

	@Override
	protected Repository<BiomecanicaPalmilha> getRepository() {
		return this.biomecanicaPalmilhaRepository;
	}

}
