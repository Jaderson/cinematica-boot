package com.cinematica.service.acompanhamentoFilho.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cinematica.framework.acompanhamento.AcompanhamentoFilho;
import com.cinematica.framework.service.impl.AbstractService;
import com.cinematica.framework.validador.Validador;
import com.cinematica.repository.Repository;
import com.cinematica.repository.acompanhamentoFilho.AcompanhamentoFilhoRepository;
import com.cinematica.service.acompanhamentoFilho.AcompanhamentoFilhoService;

@Component
public class AcompanhamentoFilhoServiceImpl extends AbstractService<AcompanhamentoFilho> implements AcompanhamentoFilhoService{

	private AcompanhamentoFilhoRepository acompanhamentoFilhoRepository;

	@Autowired
	public AcompanhamentoFilhoServiceImpl(Validador validador, AcompanhamentoFilhoRepository acompanhamentoFilhoRepository) {
		super(validador);
		this.acompanhamentoFilhoRepository = acompanhamentoFilhoRepository;
	}

	@Override
	protected Repository<AcompanhamentoFilho> getRepository() {
		return this.acompanhamentoFilhoRepository;
	}

}
