package com.cinematica.service.profissao;

import com.cinematica.framework.service.Service;
import com.cinematica.model.profissao.Profissao;

public interface ProfissaoService extends Service<Profissao>{
}
