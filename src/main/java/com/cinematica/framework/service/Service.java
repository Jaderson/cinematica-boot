package com.cinematica.framework.service;

import java.util.List;

import com.cinematica.framework.consulta.restricao.Restricoes;

public interface Service<E> {
	
	void cadastrar(E entidade);

	void alterar(E entidade);

	void excluir(E entidade);
	
	E consultarPorId(E entidade);
	
	E consultarPorId(Integer id);
	
	E consultarEntidade(final E entidade, final List<Restricoes> listaRestricoes);

	List<E> consultarTodos(E entidade);
	
	List<E> consultarTodos(E entidade, List<Restricoes> listaRestricoes, String campoOrdenacao);

	List<E> consultarTodosPaginado(E entidade, int first, int pageSize, List<Restricoes> listaRestricoes, String campoOrdenacao);
	
	Integer consultarQuantidadeRegistros(E entidade, List<Restricoes> listaRestricoes, String campoOrdenacao);

	List<E> filtro(List<Restricoes> listaRestricoes, E entidade, String campoOrdenacao);
	
	List<E> filtro(E entidade);

	List<E> consultarTodosDesc(E entidade, List<Restricoes> listaRestricoes, String campoOrdenacao);
	
}