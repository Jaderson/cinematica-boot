package com.cinematica.framework.security.exception;

public class FisioSystemAuthenticationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FisioSystemAuthenticationException(String mensagem) {
		super(mensagem);
	}


}
