package com.cinematica.framework.avaliacoes;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.empresa.SetadorEmpresa;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;
import com.cinematica.model.pessoa.Pessoa;

@Entity
@Audited
@Table(name = "avaliacao_biomecanica")
public class Biomecanica implements Serializable, Identidade, Ordenacao, SetadorEmpresa{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String tenisQueCorre;
	private String indicacao;
	private String diagnosticoMedico;
	private String objetivo;
	private String queixaPrincipal;
	private String historiaDoencaAtual;
	private String historiaPatologiaPregresa;
	private TipoMedida assimetriaMembro;
	private TipoMedida tensaoTibialAnterior;
	private TipoMedida dorsiflexaoAtiva;
	private TipoMedida gluteoMedio;
	private TipoMedida rotadoresExternos;
	private TipoMedida gluteoMaximoEncurtada;
	private Lado loungeTeste;
	private String agachamentoUnipol;
	private String agachamentoBipodal;
	private String stepDown;
	private String bancoWells;
	private Resultado oberTeste;
	private String oberTesteObs;
	private Resultado thomasTeste;
	private String thomasTesteObs;
	private String tipoPisadaDireita;
	private String tipoPisadaEsquerdo;
	private String obsTipoPisada;
	private String modeloTenis;
	private String marcaTenis;
	private String procedimentoNescessario;
	private Empresa empresa;
	private Usuario usuario;
	private Pessoa pessoa;
	
	private Set<BiomecanicaPalmilha> biomecanicaPalmilhas;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "tenis_corre")
	public String getTenisQueCorre() {
		return tenisQueCorre;
	}

	public void setTenisQueCorre(String tenisQueCorre) {
		this.tenisQueCorre = tenisQueCorre;
	}

	@Column(name = "indicacao")
	public String getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(String indicacao) {
		this.indicacao = indicacao;
	}

	@Column(name = "diagnostico_medico")
	public String getDiagnosticoMedico() {
		return diagnosticoMedico;
	}

	public void setDiagnosticoMedico(String diagnosticoMedico) {
		this.diagnosticoMedico = diagnosticoMedico;
	}

	@Column(name = "objetivo")
	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	@Column(name = "queixa_principal")
	public String getQueixaPrincipal() {
		return queixaPrincipal;
	}

	public void setQueixaPrincipal(String queixaPrincipal) {
		this.queixaPrincipal = queixaPrincipal;
	}

	@Column(name = "hda")
	public String getHistoriaDoencaAtual() {
		return historiaDoencaAtual;
	}

	public void setHistoriaDoencaAtual(String historiaDoencaAtual) {
		this.historiaDoencaAtual = historiaDoencaAtual;
	}

	@Column(name = "hpp")
	public String getHistoriaPatologiaPregresa() {
		return historiaPatologiaPregresa;
	}

	public void setHistoriaPatologiaPregresa(String historiaPatologiaPregresa) {
		this.historiaPatologiaPregresa = historiaPatologiaPregresa;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "assimetrica_membro")
	public TipoMedida getAssimetriaMembro() {
		return assimetriaMembro;
	}

	public void setAssimetriaMembro(TipoMedida assimetriaMembro) {
		this.assimetriaMembro = assimetriaMembro;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "tensao_tibial_anterior")
	public TipoMedida getTensaoTibialAnterior() {
		return tensaoTibialAnterior;
	}

	public void setTensaoTibialAnterior(TipoMedida tensaoTibialAnterior) {
		this.tensaoTibialAnterior = tensaoTibialAnterior;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "dorsiflexao_ativa")
	public TipoMedida getDorsiflexaoAtiva() {
		return dorsiflexaoAtiva;
	}

	public void setDorsiflexaoAtiva(TipoMedida dorsiflexaoAtiva) {
		this.dorsiflexaoAtiva = dorsiflexaoAtiva;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "gluteo_medio")
	public TipoMedida getGluteoMedio() {
		return gluteoMedio;
	}

	public void setGluteoMedio(TipoMedida gluteoMedio) {
		this.gluteoMedio = gluteoMedio;
	}
	@Enumerated(EnumType.STRING)
	@Column(name = "rotadores_externos")
	public TipoMedida getRotadoresExternos() {
		return rotadoresExternos;
	}

	public void setRotadoresExternos(TipoMedida rotadoresExternos) {
		this.rotadoresExternos = rotadoresExternos;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "lounge_teste")
	public Lado getLoungeTeste() {
		return loungeTeste;
	}

	public void setLoungeTeste(Lado loungeTeste) {
		this.loungeTeste = loungeTeste;
	}

	@Column(name = "agachamento_unipol")
	public String getAgachamentoUnipol() {
		return agachamentoUnipol;
	}

	public void setAgachamentoUnipol(String agachamentoUnipol) {
		this.agachamentoUnipol = agachamentoUnipol;
	}

	@Column(name = "agachamento_bipodal")
	public String getAgachamentoBipodal() {
		return agachamentoBipodal;
	}

	public void setAgachamentoBipodal(String agachamentoBipodal) {
		this.agachamentoBipodal = agachamentoBipodal;
	}

	@Column(name = "step_Down")
	public String getStepDown() {
		return stepDown;
	}

	public void setStepDown(String stepDown) {
		this.stepDown = stepDown;
	}

	@Column(name = "banco_Wells")
	public String getBancoWells() {
		return bancoWells;
	}

	public void setBancoWells(String bancoWells) {
		this.bancoWells = bancoWells;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ober_teste")
	public Resultado getOberTeste() {
		return oberTeste;
	}

	public void setOberTeste(Resultado oberTeste) {
		this.oberTeste = oberTeste;
	}

	@Column(name = "ober_observacao")
	public String getOberTesteObs() {
		return oberTesteObs;
	}

	public void setOberTesteObs(String oberTesteObs) {
		this.oberTesteObs = oberTesteObs;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "thomas_teste")
	public Resultado getThomasTeste() {
		return thomasTeste;
	}

	public void setThomasTeste(Resultado thomasTeste) {
		this.thomasTeste = thomasTeste;
	}

	@Column(name = "thomas_teste_observacao")
	public String getThomasTesteObs() {
		return thomasTesteObs;
	}

	public void setThomasTesteObs(String thomasTesteObs) {
		this.thomasTesteObs = thomasTesteObs;
	}

	@Column(name = "pisada_direita")
	public String getTipoPisadaDireita() {
		return tipoPisadaDireita;
	}

	public void setTipoPisadaDireita(String tipoPisadaDireita) {
		this.tipoPisadaDireita = tipoPisadaDireita;
	}

	@Column(name = "pisada_esquerda")
	public String getTipoPisadaEsquerdo() {
		return tipoPisadaEsquerdo;
	}

	public void setTipoPisadaEsquerdo(String tipoPisadaEsquerdo) {
		this.tipoPisadaEsquerdo = tipoPisadaEsquerdo;
	}

	@Column(name = "marca_tenis")
	public String getMarcaTenis() {
		return marcaTenis;
	}
	
	public void setMarcaTenis(String marcaTenis) {
		this.marcaTenis = marcaTenis;
	}
	
	@Column(name = "modelo_tenis")
	public String getModeloTenis() {
		return modeloTenis;
	}
	
	public void setModeloTenis(String modeloTenis) {
		this.modeloTenis = modeloTenis;
	}
	
	@Column(name = "procedimento_nescessario")
	public String getProcedimentoNescessario() {
		return procedimentoNescessario;
	}

	public void setProcedimentoNescessario(String procedimentoNescessario) {
		this.procedimentoNescessario = procedimentoNescessario;
	}
	
	@Column(name = "gluteo_maximo_encurtada")
	public TipoMedida getGluteoMaximoEncurtada() {
		return gluteoMaximoEncurtada;
	}
	
	public void setGluteoMaximoEncurtada(TipoMedida gluteoMaximoEncurtada) {
		this.gluteoMaximoEncurtada = gluteoMaximoEncurtada;
	}
	
	@Column(name = "obs_tipo_pisada")
	public String getObsTipoPisada() {
		return obsTipoPisada;
	}
	
	public void setObsTipoPisada(String obsTipoPisada) {
		this.obsTipoPisada = obsTipoPisada;
	}

	@OneToMany(mappedBy="biomecanica", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	public Set<BiomecanicaPalmilha> getBiomecanicaPalmilhas() {
		return biomecanicaPalmilhas;
	}

	public void setBiomecanicaPalmilhas(Set<BiomecanicaPalmilha> biomecanicaPalmilhas) {
		this.biomecanicaPalmilhas = biomecanicaPalmilhas;
	}
	
	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false)	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@ManyToOne
	@JoinColumn(name = "pessoa_id", nullable = false)
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	

	@Transient
	@Override
	public String getCampoOrderBy() {
		return "id";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Biomecanica))
			return false;
		Biomecanica other = (Biomecanica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
