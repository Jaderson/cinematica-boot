package com.cinematica.framework.avaliacoes;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.empresa.SetadorEmpresa;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;
import com.cinematica.model.pessoa.Pessoa;

@Entity
@Table(name="avaliacao_fisioterapia")
public class Fisioterapia implements Serializable, Identidade, Ordenacao,
		SetadorEmpresa {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String indicacao;
	private String diagnosticoMedico;
	private String objetivo;
	private String queixaPrincipal;
	private String historiaDoencaAtual;
	private String medicamento;
	private Empresa empresa;
	private Usuario usuario;
	private Pessoa pessoa;
	private String testeClinicosFuncionais;
	private String sugestao;
	private String procedimentoNescessario;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="indicacao")
	public String getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(String indicacao) {
		this.indicacao = indicacao;
	}

	@Column(name="diagnostico_medico")
	public String getDiagnosticoMedico() {
		return diagnosticoMedico;
	}

	public void setDiagnosticoMedico(String diagnosticoMedico) {
		this.diagnosticoMedico = diagnosticoMedico;
	}

	@Column(name="objetivo")
	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	@Column(name="queixa_principal")
	public String getQueixaPrincipal() {
		return queixaPrincipal;
	}

	public void setQueixaPrincipal(String queixaPrincipal) {
		this.queixaPrincipal = queixaPrincipal;
	}

	@Column(name="hda")
	public String getHistoriaDoencaAtual() {
		return historiaDoencaAtual;
	}

	public void setHistoriaDoencaAtual(String historiaDoencaAtual) {
		this.historiaDoencaAtual = historiaDoencaAtual;
	}

	@Column(name="teste_clinios_funcionais")
	public String getTesteClinicosFuncionais() {
		return testeClinicosFuncionais;
	}

	public void setTesteClinicosFuncionais(String testeClinicosFuncionais) {
		this.testeClinicosFuncionais = testeClinicosFuncionais;
	}

	@Column(name="sugestao")
	public String getSugestao() {
		return sugestao;
	}

	public void setSugestao(String sugestao) {
		this.sugestao = sugestao;
	}

	@Column(name="procedimento_nescessario")
	public String getProcedimentoNescessario() {
		return procedimentoNescessario;
	}

	public void setProcedimentoNescessario(String procedimentoNescessario) {
		this.procedimentoNescessario = procedimentoNescessario;
	}
	
	@Column(name="medicamento")
	public String getMedicamento() {
		return medicamento;
	}
	
	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}

	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false)
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@ManyToOne
	@JoinColumn(name = "pessoa_id", nullable = false)
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Transient
	@Override
	public String getCampoOrderBy() {
		return "id";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Fisioterapia))
			return false;
		Fisioterapia other = (Fisioterapia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
