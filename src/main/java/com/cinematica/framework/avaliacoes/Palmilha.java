package com.cinematica.framework.avaliacoes;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.empresa.SetadorEmpresa;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;


@Entity
@Audited
@Table(name = "palmilha")
public class Palmilha implements Serializable,Identidade,Ordenacao,SetadorEmpresa{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Lado lado;
	private Integer numeracao;
	private String cobertura;
	private String borda;
	private String retrope;
	private String antepe;
	private String altura;
	private String arco;
	private String suporte;
	private String absorcao;
	private String pad;
	private Empresa empresa;
	
	public Palmilha(){}
	
	public Palmilha(Palmilha palmilha){}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "lado")
	public Lado getLado() {
		return lado;
	}

	public void setLado(Lado lado) {
		this.lado = lado;
	}

	@Column(name = "palmilha_numeracao")
	public Integer getNumeracao() {
		return numeracao;
	}

	public void setNumeracao(Integer numeracao) {
		this.numeracao = numeracao;
	}

	@Column(name = "cobertura")
	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	@Column(name = "borda")
	public String getBorda() {
		return borda;
	}

	public void setBorda(String borda) {
		this.borda = borda;
	}

	@Column(name = "retrope")
	public String getRetrope() {
		return retrope;
	}

	public void setRetrope(String retrope) {
		this.retrope = retrope;
	}

	@Column(name = "antepe")
	public String getAntepe() {
		return antepe;
	}

	public void setAntepe(String antepe) {
		this.antepe = antepe;
	}

	@Column(name = "palmilha_altura")
	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	@Column(name = "arco")
	public String getArco() {
		return arco;
	}

	public void setArco(String arco) {
		this.arco = arco;
	}

	@Column(name = "suporte")
	public String getSuporte() {
		return suporte;
	}

	public void setSuporte(String suporte) {
		this.suporte = suporte;
	}

	@Column(name = "absorcao")
	public String getAbsorcao() {
		return absorcao;
	}

	public void setAbsorcao(String absorcao) {
		this.absorcao = absorcao;
	}

	@Column(name = "pad")
	public String getPad() {
		return pad;
	}

	public void setPad(String pad) {
		this.pad = pad;
	}
	
	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false)	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public String getCampoOrderBy() {
		return "id";
	}
}
