package com.cinematica.framework.avaliacoes;

public enum Resultado {

	Positivo("Positivo"), 
	Negativo("Negativo");

	private String resultado;

	Resultado(String resultado) {
		this.resultado = resultado;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
}
