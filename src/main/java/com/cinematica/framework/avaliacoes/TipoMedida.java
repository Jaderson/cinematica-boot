package com.cinematica.framework.avaliacoes;

public enum TipoMedida {

	Maior("Maior"),
	Menor("Menor"),
	Normal("Normal"),
	Alterada("Alterada");
	
	private String valor;

	TipoMedida(String valor){
		this.valor = valor;
	}
	public String getValor() {
		return valor;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
}
