package com.cinematica.framework.avaliacoes;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;

@Entity
@Audited
@Table(name = "biomecanica_palmilha")
public class BiomecanicaPalmilha implements Serializable ,Identidade, Ordenacao{

private static final long serialVersionUID = 1L;
	
	private Long id;
	private Biomecanica biomecanica;
	private Palmilha palmilha;
	
	public BiomecanicaPalmilha(){ };
	
	public BiomecanicaPalmilha(Biomecanica biomecanica, Palmilha palmilha) {
		this.palmilha = palmilha;
		this.biomecanica = biomecanica;
	};
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "palmilha_id", nullable = false)
	public Palmilha getPalmilha() {
		return palmilha;
	}

	public void setPalmilha(Palmilha palmilha) {
		this.palmilha = palmilha;
	}
	
	@ManyToOne
	@JoinColumn(name = "biomecanica_id", nullable = false)
	public Biomecanica getBiomecanica() {
		return biomecanica;
	}
	
	public void setBiomecanica(Biomecanica biomecanica) {
		this.biomecanica = biomecanica;
	}
	
	@Transient
	public String getCampoOrderBy() {
		return "id";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((biomecanica == null) ? 0 : biomecanica.hashCode());
		result = prime * result
				+ ((palmilha == null) ? 0 : palmilha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BiomecanicaPalmilha))
			return false;
		BiomecanicaPalmilha other = (BiomecanicaPalmilha) obj;
		if (biomecanica == null) {
			if (other.biomecanica != null)
				return false;
		} else if (!biomecanica.equals(other.biomecanica))
			return false;
		if (palmilha == null) {
			if (other.palmilha != null)
				return false;
		} else if (!palmilha.equals(other.palmilha))
			return false;
		return true;
	}
	
}
