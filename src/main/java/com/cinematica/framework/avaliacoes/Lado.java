package com.cinematica.framework.avaliacoes;

public enum Lado {

	Direita("Direita"),
	Esquerda("Esquerda"),
	Bilateral("Bilateral");
	
	private String valor;

	Lado(String valor){
		this.valor = valor;
	}
	public String getValor() {
		return valor;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
}
