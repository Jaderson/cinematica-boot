package com.cinematica.framework.acompanhamento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;
import com.cinematica.model.pessoa.Pessoa;

@Entity
@Audited
@Table(name = "acompanhamento_filho")
public class AcompanhamentoFilho implements Serializable, Identidade, Ordenacao {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer secao;
	private String observacao;
	private Empresa empresa;
	private Pessoa pessoa;
	private Date dataSecao;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="secao")
	public Integer getSecao() {
		return secao;
	}

	public void setSecao(Integer secao) {
		this.secao = secao;
	}

	@Column(name="observacao")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_secao", nullable = false)
	public Date getDataSecao() {
		return dataSecao;
	}
	
	public void setDataSecao(Date dataSecao) {
		this.dataSecao = dataSecao;
	}
	
	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false)
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@ManyToOne
	@JoinColumn(name = "pessoa_id", nullable = false)
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Transient
	@Override
	public String getCampoOrderBy() {
		return "id";
	}
}
