package com.cinematica.framework.acompanhamento;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.empresa.SetadorEmpresa;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;
import com.cinematica.model.pessoa.Pessoa;

@Entity
@Audited
@Table(name = "acompanhamento_pessoa")
public class AcompanhamentoPessoa implements  Serializable, Identidade, Ordenacao,SetadorEmpresa{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Pessoa pessoa;
	private Empresa empresa;
	private Set<AcompanhamentoFilho> acompanhamento;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "pessoa_id", nullable = false)
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false)
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@OneToMany(mappedBy="pessoa", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	public Set<AcompanhamentoFilho> getAcompanhamento() {
		return acompanhamento;
	}
	
	public void setAcompanhamento(Set<AcompanhamentoFilho> acompanhamento) {
		this.acompanhamento = acompanhamento;
	}

	@Transient
	@Override
	public String getCampoOrderBy() {
		return "id";
	}

}
