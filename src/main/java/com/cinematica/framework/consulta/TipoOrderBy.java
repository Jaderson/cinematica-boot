package com.cinematica.framework.consulta;

public enum TipoOrderBy {

	ASC, DESC;
}
