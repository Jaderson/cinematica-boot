package com.cinematica.framework.consulta.restricao;

import static com.cinematica.framework.consulta.Consulta.DOIS_PONTOS;
import static com.cinematica.framework.consulta.Consulta.ESPACO;
import static com.cinematica.framework.consulta.Consulta.FINAL;
import static com.cinematica.framework.consulta.Consulta.INICIAL;

import com.cinematica.framework.consulta.TipoConectivo;

public class RestricaoBetWeen extends AbstractRestricao{
	
	@Override
	public void montarRetricaoParticular(Restricoes restricoes) {
		restricoes.seAtributoCompostoRetiraPonto();
		getSelect().append(DOIS_PONTOS)
		.append(restricoes.getNome()+INICIAL)
        .append(ESPACO)
        .append(TipoConectivo.AND.toString())
        .append(ESPACO)
        .append(DOIS_PONTOS)
		.append(restricoes.getNome()+FINAL);
	}
}
