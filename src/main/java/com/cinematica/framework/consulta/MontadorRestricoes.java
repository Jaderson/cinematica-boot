package com.cinematica.framework.consulta;

import com.cinematica.framework.consulta.restricao.Restricoes;

public interface MontadorRestricoes {

	String montarRestricao(Restricoes restricoes);
	
}
