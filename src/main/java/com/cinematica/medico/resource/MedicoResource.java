package com.cinematica.medico.resource;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cinematica.exception.CinematicaException;
import com.cinematica.exception.CinematicaExceptionHandler.Erro;
import com.cinematica.framework.json.JsonUtil;
import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.model.medico.Medico;
import com.cinematica.security.config.RecursoCriadoEvent;
import com.cinematica.service.especialidade.EspecialidadeService;
import com.cinematica.service.medico.MedicoService;

@RestController
@RequestMapping("medico")
@Transactional
@CrossOrigin
public class MedicoResource {

	@Autowired
	private MedicoService medicoService;
	
	@Autowired
	private EspecialidadeService especialidadeService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource messageSource;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_MEDICO_CONSULTAR')")
	public ResponseEntity<String> listarTodos() {
		List<Medico> listMedico = this.medicoService.consultarTodos(new Medico());
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listMedico);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ROLE_MEDICO_INSERIR')")
	public ResponseEntity<String> cadastrar(@Valid @RequestBody Medico entidade, HttpServletResponse response) {
		this.medicoService.cadastrar(entidade);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, entidade.getId()));
		return new ResponseEntity<>(jsonObject, HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_MEDICO_CONSULTAR')")
	public ResponseEntity<String> consultarPorId(@PathVariable Integer id) {
		Medico entidade = this.medicoService.consultarPorId(id);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}
	
	@GetMapping("/especialidade/{id}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('ROLE_ESPECIALIDADE_CONSULTAR')")
	public ResponseEntity<String>  consultarEspecialidadePorId(@PathVariable Integer id) {
		Especialidade entidade = this.especialidadeService.consultarPorId(id);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_MEDICO_EXCLUIR')")
	public ResponseEntity<String> excluir(@PathVariable("id") Integer id) {
		Medico entidade = this.medicoService.consultarPorId(id);
		this.medicoService.excluir(entidade);
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return new ResponseEntity<String>(jsonObject, HttpStatus.OK);
	}

	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_MEDICO_ALTERAR')")
	public ResponseEntity<String> atualizar(@RequestBody Medico entidade) {

		Medico medico = this.medicoService.consultarPorId(entidade.getId());
		Especialidade especialidade = this.especialidadeService.consultarPorId(entidade.getEspecialidade().getId());
		medico.setNome(entidade.getNome());
		medico.setEmail(entidade.getEmail());
		medico.setTelefone(entidade.getTelefone());
		medico.setEspecialidade(especialidade);

		this.medicoService.alterar(medico);
		String jsonObject = JsonUtil.convertObjectToJsonString(medico);
		return new ResponseEntity<>(jsonObject, HttpStatus.OK);
	}
	
	@ExceptionHandler({ CinematicaException.class })
	public ResponseEntity<Object> CinematicaException(CinematicaException ex) {
		String mensagemUsuario = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
}
