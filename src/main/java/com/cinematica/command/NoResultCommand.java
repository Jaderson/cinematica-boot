package com.cinematica.command;


public interface NoResultCommand {

	Object execute();
	
}
