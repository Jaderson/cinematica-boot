package com.cinematica.command;

import javax.persistence.NoResultException;

public class CommandRepositoryExecuter {

	public static Object executar(NoResultCommand command) {
		try {
			return command.execute();
		} catch (NoResultException e) {
			return null;
		}
	}
}