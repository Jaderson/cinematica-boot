package com.cinematica.exception;

public class MedicoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MedicoException() {
		super();
	}

	public MedicoException(String message) {
		super(message);
	}

}
