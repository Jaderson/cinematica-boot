package com.cinematica.repository.command;

public interface CommandSingleResult<E> {

	E execute();

}
