package com.cinematica.repository.profissao.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.profissao.Profissao;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.profissao.ProfissaoRepository;

@Component
@SuppressWarnings("unchecked")
public class ProfissaoRepositoryImpl extends AbstractRepository<Profissao> implements ProfissaoRepository{

}
