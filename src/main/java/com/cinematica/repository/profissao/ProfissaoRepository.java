package com.cinematica.repository.profissao;

import com.cinematica.model.profissao.Profissao;
import com.cinematica.repository.Repository;

public interface ProfissaoRepository extends Repository<Profissao>{

}
