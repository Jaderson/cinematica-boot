package com.cinematica.repository.medico.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.medico.Medico;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.medico.MedicoRepository;

@Component
@SuppressWarnings("unchecked")
public class MedicoRepositoryImpl extends AbstractRepository<Medico> implements MedicoRepository{

}
