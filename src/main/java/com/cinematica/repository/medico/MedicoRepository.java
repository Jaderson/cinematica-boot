package com.cinematica.repository.medico;

import com.cinematica.model.medico.Medico;
import com.cinematica.repository.Repository;

public interface MedicoRepository extends Repository<Medico>{

}
