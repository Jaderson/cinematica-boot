package com.cinematica.repository.usuarioPerfil;

import java.util.List;

import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.model.autenticacao.UsuarioPerfil;
import com.cinematica.repository.Repository;

public interface UsuarioPerfilRepository extends Repository<UsuarioPerfil>{

	List<UsuarioPerfil> consultarPorIdUsuario(Usuario usuario);

}
