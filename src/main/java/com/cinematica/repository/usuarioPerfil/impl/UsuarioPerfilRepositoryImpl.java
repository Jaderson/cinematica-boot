package com.cinematica.repository.usuarioPerfil.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.model.autenticacao.UsuarioPerfil;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.usuarioPerfil.UsuarioPerfilRepository;

@Component
@SuppressWarnings("unchecked")
public class UsuarioPerfilRepositoryImpl extends AbstractRepository<UsuarioPerfil> implements UsuarioPerfilRepository{

	@Override
	public List<UsuarioPerfil> consultarPorIdUsuario(Usuario usuario) {
		return this.getEntityManager().createQuery(" select up from UsuarioPerfil up "
				+ " where up.usuario.id = :idUsuario ")
				.setParameter("idUsuario", usuario.getId())
				.getResultList();
	}

}
