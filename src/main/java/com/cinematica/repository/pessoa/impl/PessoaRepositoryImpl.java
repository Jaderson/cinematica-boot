package com.cinematica.repository.pessoa.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cinematica.model.pessoa.Pessoa;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.pessoa.PessoaRepository;

@Component
@SuppressWarnings("unchecked")
public class PessoaRepositoryImpl extends AbstractRepository<Pessoa> implements PessoaRepository{

	@Override
	public List<Pessoa> consultarAniversariante() {
		return this.getEntityManager().createQuery("SELECT p FROM Pessoa p "
				+ " WHERE MONTH(p.dataNascimento) = MOD(MONTH(CURDATE()), 12) "
				+ " order by day(p.dataNascimento) desc ")
				.getResultList();
	}

}
