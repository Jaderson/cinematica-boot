package com.cinematica.repository.pessoa;

import java.util.List;

import com.cinematica.model.pessoa.Pessoa;
import com.cinematica.repository.Repository;

public interface PessoaRepository extends Repository<Pessoa>{

	List<Pessoa> consultarAniversariante();
}
