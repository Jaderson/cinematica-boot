package com.cinematica.repository.palmilha.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.Palmilha;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.palmilha.PalmilhaRepository;

@Component
@SuppressWarnings("unchecked")
public class PalmilhaRepositoryImpl extends AbstractRepository<Palmilha> implements PalmilhaRepository{

}
