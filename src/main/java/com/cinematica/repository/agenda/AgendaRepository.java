package com.cinematica.repository.agenda;

import java.util.Date;
import java.util.List;

import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.repository.Repository;

public interface AgendaRepository extends Repository<Agenda>{

	List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente);
	
	void consultarSeExisteEspecialidade(Especialidade entidade);
	
	List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa);
}
