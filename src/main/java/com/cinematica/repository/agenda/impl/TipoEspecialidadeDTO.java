package com.cinematica.repository.agenda.impl;

public class TipoEspecialidadeDTO {
	
	private String nome;
	private Long contador;

	public TipoEspecialidadeDTO() {
	}

	public TipoEspecialidadeDTO(String nome, Long contador) {
		super();
		this.nome = nome;
		this.contador = contador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getContador() {
		return contador;
	}

	public void setContador(Long contador) {
		this.contador = contador;
	}
}
