package com.cinematica.repository.agenda.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cinematica.exception.EspecialidadeException;
import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.agenda.AgendaRepository;

@Component
@SuppressWarnings("unchecked")
public class AgendaRepositoryImpl extends AbstractRepository<Agenda> implements AgendaRepository{
	
	@Override
	public List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente) {
		return this.getEntityManager().createQuery("select a from Agenda a "
				+ " inner join a.pessoa p "
				+ " where (a.dataInicio between :dataInicio and :dataFim "
				+ " or a.dataFim between :dataInicio and :dataFim ) "
				+ " and a.falta = 1 "
				+ " and p.id = a.pessoa.id "
				+ " order by a.dataInicio desc ")
				.setParameter("dataInicio", pegarPrimerioDataMesCorrente)
				.setParameter("dataFim", pegarUltimaDataMesCorrente)
				.getResultList();
	}

	@Override
	public void consultarSeExisteEspecialidade(Especialidade entidade) {
		if (!this.getEntityManager().createQuery("select a from Agenda a "
				+ " inner join a.especialidade esp "
				+ " where esp.id = a.especialidade.id  "
				+ " and a.especialidade.id = :id")
				.setParameter("id", entidade.getId())
				.getResultList().isEmpty()) {
			throw new EspecialidadeException("existem_consultas_que_utilizam_esta_especialidade");
		}
	}

	@Override
	public List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa) {
		List<Object[]> results = this.getEntityManager().createQuery("select esp.descricao, count(a.id) from Agenda a "
				+ " inner join a.especialidade esp "
				+ " 	where (a.dataInicio between :dataInicio and :dataFim "
				+ " or a.dataFim between :dataInicio and :dataFim ) "
				+ " and esp.id = a.especialidade.id "
				+ " and a.empresa.id = :idEmpresa"
				+ " group by 1")
				.setParameter("dataInicio", pegarPrimerioDataMesCorrente)
				.setParameter("dataFim", pegarUltimaDataMesCorrente)
				.setParameter("idEmpresa", idEmpresa)
				.getResultList();
		return  results;
	}

}
