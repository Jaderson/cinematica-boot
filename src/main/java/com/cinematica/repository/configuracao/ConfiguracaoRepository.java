package com.cinematica.repository.configuracao;

import java.util.List;

import com.cinematica.framework.configuracao.Configuracao;
import com.cinematica.repository.Repository;

public interface ConfiguracaoRepository extends Repository<Configuracao>{

	List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int idSemana, Integer idEmpresa);

}
