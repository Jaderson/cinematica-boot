package com.cinematica.repository.configuracao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cinematica.framework.configuracao.Configuracao;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.configuracao.ConfiguracaoRepository;

@Component
@SuppressWarnings("unchecked")
public class ConfiguracaoRepositoryImpl extends AbstractRepository<Configuracao> implements ConfiguracaoRepository{

	@Override
	public List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int idSemana, Integer idEmpresa) {
		String parametro = null;
		String concatenar = null;

		if(Semana.Domingo.getValue() == idSemana){
			concatenar = " and c.domingo = :idDomingo ";
			parametro = "idDomingo";
		}else if(Semana.Segunda.getValue() == idSemana){
			concatenar = " and c.segunda = :idSegunda ";
			parametro = "idSegunda";
		}else if(Semana.Terca.getValue() == idSemana){
			concatenar = " and c.terca   = :idTerca ";
			parametro = "idTerca";
		}else if(Semana.Quarta.getValue() == idSemana){
			concatenar = " and c.quarta  = :idQuarta ";
			parametro = "idQuarta";
		}else if(Semana.Quinta.getValue() == idSemana){
			concatenar = " and c.quinta  = :idQuinta ";
			parametro = "idQuinta";
		}else if(Semana.Sexta.getValue() == idSemana){
			concatenar = " and c.sexta   = :idSexta ";
			parametro = "idSexta";
		}else if(Semana.Sabado.getValue() == idSemana){
			concatenar = " and c.sabado  = :idSabado ";
			parametro = "idSabado";
		}
		
		List<Configuracao> listPessoas = this.getEntityManager()
				.createQuery("select c from Configuracao c, Empresa e where c.empresa.id = e.id and e.id = :idEmpresa".concat(concatenar))
				.setParameter(parametro,isPesquisa)
				.setParameter("idEmpresa",idEmpresa)
				.getResultList();
		  
		  return listPessoas;
	}
}
