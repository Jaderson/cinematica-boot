package com.cinematica.repository.configuracao.impl;

public enum Semana {
	
	Domingo(1),
	Segunda(2),
	Terca(3),
	Quarta(4),
	Quinta(5),
	Sexta(6),
	Sabado(7);
	
	
	private Integer value;

	private Semana() {}
	
	private Semana(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
