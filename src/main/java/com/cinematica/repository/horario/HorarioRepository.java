package com.cinematica.repository.horario;

import java.util.Date;
import java.util.List;

import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.agenda.Horario;
import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.repository.Repository;

public interface HorarioRepository extends Repository<Horario>{

	List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade);
	
	List<Horario> consultarHorarioPorData(Integer idFuncionario, Date dataInicio);
}
