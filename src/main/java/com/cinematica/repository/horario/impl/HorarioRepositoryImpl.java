package com.cinematica.repository.horario.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cinematica.model.agenda.Agenda;
import com.cinematica.model.agenda.Horario;
import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.horario.HorarioRepository;

@Component
@SuppressWarnings("unchecked")
public class HorarioRepositoryImpl extends AbstractRepository<Horario> implements HorarioRepository{

	@Override
	public List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade) {
		
		return this.getEntityManager().createQuery("select hd from HorarioDisponivel hd"
				+ " inner join hd.horario h "
				+ " inner join hd.dataFalta df "
				+ " inner join df.configuracao c "
				+ " where c.pessoa.id = :idPessoa "
				+ " and df.dataFalta = :dataFalta " )
				.setParameter("idPessoa", entidade.getFuncionario().getId())
				.setParameter("dataFalta", entidade.getDataInicio())
				.getResultList();
	}

	@Override
	public List<Horario> consultarHorarioPorData(Integer idFuncionario, Date dataInicio) {
		return this.getEntityManager().createQuery("select hr from Horario hr "
				+ " where id not in (select h.id from HorarioDisponivel hd "
				+ " inner join hd.horario h "
				+ " inner join hd.dataFalta df "
				+ " inner join df.configuracao c "
				+ " where c.pessoa.id = :idPessoa "
				+ " and df.dataFalta = :dataFalta )" )
				.setParameter("idPessoa", idFuncionario)
				.setParameter("dataFalta", dataInicio)
				.getResultList();
	}
}
