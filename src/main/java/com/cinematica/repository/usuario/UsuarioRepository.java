package com.cinematica.repository.usuario;

import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.repository.Repository;

public interface UsuarioRepository extends Repository<Usuario>{

	Usuario consultarUsuarioPorLogin(String username);
	
}
