package com.cinematica.repository.usuario.impl;

import org.springframework.stereotype.Component;

import com.cinematica.command.CommandRepositoryExecuter;
import com.cinematica.command.NoResultCommand;
import com.cinematica.framework.consulta.ConsultaJPA;
import com.cinematica.framework.consulta.restricao.Restricoes;
import com.cinematica.model.autenticacao.Usuario;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.usuario.UsuarioRepository;

@Component
public class UsuarioRepositoryImpl extends AbstractRepository<Usuario>
		implements UsuarioRepository {

	@Override
	public Usuario consultarUsuarioPorLogin(final String username) {
		return (Usuario) CommandRepositoryExecuter
				.executar(new NoResultCommand() {

					@Override
					public Object execute() {
						return new ConsultaJPA<Usuario>(getEntityManager(),
								new Usuario().getClass().getSimpleName())
								.addParametroQuery(
										Restricoes.igual("login", username))
								.singleResult();
					}
				});
	}

}
