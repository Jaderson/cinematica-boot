package com.cinematica.repository.perfil.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.perfil.PerfilRepository;

@Component
@SuppressWarnings("unchecked")
public class PerfilRepositoryImpl extends AbstractRepository<Perfil> implements PerfilRepository{

}
