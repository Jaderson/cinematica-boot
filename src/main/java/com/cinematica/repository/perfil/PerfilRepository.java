package com.cinematica.repository.perfil;

import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.repository.Repository;

public interface PerfilRepository extends Repository<Perfil>{

}
