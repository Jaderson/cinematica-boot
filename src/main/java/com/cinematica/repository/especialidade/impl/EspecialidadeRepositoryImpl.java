package com.cinematica.repository.especialidade.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.especialidade.EspecialidadeRepository;

@Component
@SuppressWarnings("unchecked")
public class EspecialidadeRepositoryImpl extends AbstractRepository<Especialidade> implements EspecialidadeRepository{

}
