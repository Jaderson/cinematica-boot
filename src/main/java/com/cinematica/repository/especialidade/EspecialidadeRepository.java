package com.cinematica.repository.especialidade;

import com.cinematica.model.especialidade.Especialidade;
import com.cinematica.repository.Repository;

public interface EspecialidadeRepository extends Repository<Especialidade>{

}
