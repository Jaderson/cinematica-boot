package com.cinematica.repository.categoria.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.tipoCategoria.Categoria;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.categoria.CategoriaRepository;

@Component
@SuppressWarnings("unchecked")
public class CategoriaRepositoryImpl extends AbstractRepository<Categoria> implements CategoriaRepository{

}
