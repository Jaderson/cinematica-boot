package com.cinematica.repository.categoria;

import com.cinematica.framework.tipoCategoria.Categoria;
import com.cinematica.repository.Repository;

public interface CategoriaRepository extends Repository<Categoria>{

}
