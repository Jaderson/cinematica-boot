package com.cinematica.repository.horarioDisponivel.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.horarioDisponivel.HorarioDisponivelRepository;

@Component
@SuppressWarnings("unchecked")
public class HorarioDisponivelRepositoryImpl extends AbstractRepository<HorarioDisponivel> implements HorarioDisponivelRepository{

}
