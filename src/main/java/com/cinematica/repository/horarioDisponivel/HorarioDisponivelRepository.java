package com.cinematica.repository.horarioDisponivel;

import com.cinematica.model.agenda.HorarioDisponivel;
import com.cinematica.repository.Repository;

public interface HorarioDisponivelRepository extends Repository<HorarioDisponivel>{

}
