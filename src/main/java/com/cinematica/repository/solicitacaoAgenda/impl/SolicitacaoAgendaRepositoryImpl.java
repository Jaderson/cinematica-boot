package com.cinematica.repository.solicitacaoAgenda.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.solicitacaoAgenda.SolicitacaoAgendaRepository;

@Component
@SuppressWarnings("unchecked")
public class SolicitacaoAgendaRepositoryImpl extends AbstractRepository<SolicitacaoAgenda> implements SolicitacaoAgendaRepository{

}
