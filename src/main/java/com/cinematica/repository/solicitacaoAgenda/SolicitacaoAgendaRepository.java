package com.cinematica.repository.solicitacaoAgenda;

import com.cinematica.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.cinematica.repository.Repository;

public interface SolicitacaoAgendaRepository extends Repository<SolicitacaoAgenda>{

}
