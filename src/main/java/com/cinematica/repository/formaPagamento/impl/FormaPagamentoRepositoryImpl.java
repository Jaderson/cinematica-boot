package com.cinematica.repository.formaPagamento.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.formaPagamento.FormaPagamento;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.formaPagamento.FormaPagamentoRepository;

@Component
@SuppressWarnings("unchecked")
public class FormaPagamentoRepositoryImpl extends AbstractRepository<FormaPagamento> implements FormaPagamentoRepository{

}
