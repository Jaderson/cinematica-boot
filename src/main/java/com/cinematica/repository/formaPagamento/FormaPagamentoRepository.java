package com.cinematica.repository.formaPagamento;

import com.cinematica.framework.formaPagamento.FormaPagamento;
import com.cinematica.repository.Repository;

public interface FormaPagamentoRepository extends Repository<FormaPagamento>{

}
