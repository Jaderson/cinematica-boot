package com.cinematica.repository.fluxoCaixa.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.caixa.TipoLancamento;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.fluxoCaixa.FluxoCaixaRepository;

@Component
@SuppressWarnings("unchecked")
public class FluxoCaixaRepositoryImpl extends AbstractRepository<FluxoCaixa> implements FluxoCaixaRepository{

	@Override
	public BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia, Date geraDataComUltimoDia,Integer idEmpresa,TipoLancamento tipoLancamento) {
		return (BigDecimal) getEntityManager()
				.createQuery(
						" select sum(fc.valor) from FluxoCaixa fc "
								+ " where fc.empresa.id = :idEmpresa "
								+ " and fc.dataLancamento between :dataInicioLancamento and :dataFimLancamento "
								+ " and fc.tipoLancamento = :tipoLancamento "
								+ " order by 1 desc ")
				.setParameter("idEmpresa", idEmpresa)
				.setParameter("dataInicioLancamento", geraDataComPrimeiroDia)
				.setParameter("dataFimLancamento", geraDataComUltimoDia)
				.setParameter("tipoLancamento", tipoLancamento)
				.getSingleResult();
	}

}
