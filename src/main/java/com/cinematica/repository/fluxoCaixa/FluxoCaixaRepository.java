package com.cinematica.repository.fluxoCaixa;

import java.math.BigDecimal;
import java.util.Date;

import com.cinematica.framework.caixa.FluxoCaixa;
import com.cinematica.framework.caixa.TipoLancamento;
import com.cinematica.repository.Repository;

public interface FluxoCaixaRepository extends Repository<FluxoCaixa>{

	BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia,Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento);

}
