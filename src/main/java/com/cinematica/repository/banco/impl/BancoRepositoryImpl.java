package com.cinematica.repository.banco.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.banco.Banco;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.banco.BancoRepository;

@Component
@SuppressWarnings("unchecked")
public class BancoRepositoryImpl extends AbstractRepository<Banco> implements BancoRepository{

}
