package com.cinematica.repository.banco;

import com.cinematica.framework.banco.Banco;
import com.cinematica.repository.Repository;

public interface BancoRepository extends Repository<Banco>{

}
