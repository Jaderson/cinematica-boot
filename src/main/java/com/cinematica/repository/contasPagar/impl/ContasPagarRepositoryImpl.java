package com.cinematica.repository.contasPagar.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.contasPagar.ContasPagar;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.contasPagar.ContasPagarRepository;

@Component
@SuppressWarnings("unchecked")
public class ContasPagarRepositoryImpl extends AbstractRepository<ContasPagar> implements ContasPagarRepository{

}
