package com.cinematica.repository.contasPagar;

import com.cinematica.framework.contasPagar.ContasPagar;
import com.cinematica.repository.Repository;

public interface ContasPagarRepository extends Repository<ContasPagar>{

}
