package com.cinematica.repository.dataFalta;

import java.util.List;

import com.cinematica.framework.configuracao.Configuracao;
import com.cinematica.model.agenda.DataFalta;
import com.cinematica.repository.Repository;

public interface DataFaltaRepository extends Repository<DataFalta>{

	List<DataFalta> consultarDatasPorConfiguracao(Configuracao entidade);
	
	List<DataFalta> consultarDatasPorConfiguracaoComHorario(Configuracao entidade);

}
