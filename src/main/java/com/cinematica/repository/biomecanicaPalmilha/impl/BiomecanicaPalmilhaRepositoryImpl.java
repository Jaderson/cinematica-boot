package com.cinematica.repository.biomecanicaPalmilha.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.BiomecanicaPalmilha;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.biomecanicaPalmilha.BiomecanicaPalmilhaRepository;

@Component
@SuppressWarnings("unchecked")
public class BiomecanicaPalmilhaRepositoryImpl extends AbstractRepository<BiomecanicaPalmilha> implements BiomecanicaPalmilhaRepository{

}
