package com.cinematica.repository.empresa;

import com.cinematica.model.empresa.Empresa;
import com.cinematica.repository.Repository;

public interface EmpresaRepository extends Repository<Empresa>{
}
