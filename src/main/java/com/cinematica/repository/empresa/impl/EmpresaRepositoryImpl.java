package com.cinematica.repository.empresa.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.empresa.Empresa;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.empresa.EmpresaRepository;

@Component
@SuppressWarnings("unchecked")
public class EmpresaRepositoryImpl extends AbstractRepository<Empresa> implements EmpresaRepository{
}
