package com.cinematica.repository.perfilPermissao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.perfilPermissao.PerfilPermissaoRepository;

@Component
@SuppressWarnings("unchecked")
public class PerfilPermissaoRepositoryImpl extends
		AbstractRepository<PerfilPermissao> implements
		PerfilPermissaoRepository {
	
	public List<PerfilPermissao> consultarPerfilPermissaoPorPerfil(Perfil entidade) {
		return this.getEntityManager().createQuery(" select p from PerfilPermissao p"
				+ " where p.perfil.id = :idPerfil ")
				.setParameter("idPerfil", entidade.getId())
				.getResultList();
	}

	@Override
	public PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao) {
		return (PerfilPermissao) getEntityManager()
				.createQuery(
						"select pf from PerfilPermissao pf where pf.perfil.id = :perfil "
								+ "and pf.permissao.id = :permissao")
				.setParameter("perfil", perfil.getId())
				.setParameter("permissao", permissao.getId()).getSingleResult();
	}

}
