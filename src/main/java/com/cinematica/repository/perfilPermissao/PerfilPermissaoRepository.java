package com.cinematica.repository.perfilPermissao;

import java.util.List;

import com.cinematica.model.autenticacao.Perfil;
import com.cinematica.model.autenticacao.PerfilPermissao;
import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.repository.Repository;

public interface PerfilPermissaoRepository extends Repository<PerfilPermissao> {

	PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(Perfil perfil,
			Permissao permissao);
	List<PerfilPermissao> consultarPerfilPermissaoPorPerfil(Perfil perfil);
}
