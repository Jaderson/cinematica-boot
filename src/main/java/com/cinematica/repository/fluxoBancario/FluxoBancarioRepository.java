package com.cinematica.repository.fluxoBancario;

import com.cinematica.framework.fluxoBancario.FluxoBancario;
import com.cinematica.repository.Repository;

public interface FluxoBancarioRepository extends Repository<FluxoBancario>{

}
