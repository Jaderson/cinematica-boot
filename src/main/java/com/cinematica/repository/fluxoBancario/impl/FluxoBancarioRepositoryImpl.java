package com.cinematica.repository.fluxoBancario.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.fluxoBancario.FluxoBancario;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.fluxoBancario.FluxoBancarioRepository;

@Component
@SuppressWarnings("unchecked")
public class FluxoBancarioRepositoryImpl extends AbstractRepository<FluxoBancario> implements FluxoBancarioRepository{

}
