package com.cinematica.repository.objeto;

import com.cinematica.model.autenticacao.Objeto;
import com.cinematica.repository.Repository;

public interface ObjetoRepository extends Repository<Objeto>{

}
