package com.cinematica.repository.objeto.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.autenticacao.Objeto;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.objeto.ObjetoRepository;

@Component
@SuppressWarnings("unchecked")
public class ObjetoRepositoryImpl extends AbstractRepository<Objeto> implements ObjetoRepository{

}
