package com.cinematica.repository.permissao.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.permissao.PermissaoRepository;

@Component
@SuppressWarnings("unchecked")
public class PermissaoRepositoryImpl extends AbstractRepository<Permissao> implements PermissaoRepository{

}
