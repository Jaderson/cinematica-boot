package com.cinematica
.repository.permissao;

import com.cinematica.model.autenticacao.Permissao;
import com.cinematica.repository.Repository;

public interface PermissaoRepository extends Repository<Permissao>{

}
