package com.cinematica.repository.acompanhamentoFilho.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.acompanhamento.AcompanhamentoFilho;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.acompanhamentoFilho.AcompanhamentoFilhoRepository;

@Component
@SuppressWarnings("unchecked")
public class AcompanhamentoFilhoRepositoryImpl extends AbstractRepository<AcompanhamentoFilho> implements AcompanhamentoFilhoRepository{

}
