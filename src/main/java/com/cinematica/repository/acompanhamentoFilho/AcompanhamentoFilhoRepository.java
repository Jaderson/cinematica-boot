package com.cinematica.repository.acompanhamentoFilho;

import com.cinematica.framework.acompanhamento.AcompanhamentoFilho;
import com.cinematica.repository.Repository;

public interface AcompanhamentoFilhoRepository extends Repository<AcompanhamentoFilho>{

}
