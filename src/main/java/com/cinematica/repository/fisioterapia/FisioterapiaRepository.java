package com.cinematica.repository.fisioterapia;

import com.cinematica.framework.avaliacoes.Fisioterapia;
import com.cinematica.repository.Repository;

public interface FisioterapiaRepository extends Repository<Fisioterapia>{

}
