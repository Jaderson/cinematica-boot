package com.cinematica.repository.fisioterapia.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.Fisioterapia;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.fisioterapia.FisioterapiaRepository;

@Component
@SuppressWarnings("unchecked")
public class FisioterapiaRepositoryImpl extends AbstractRepository<Fisioterapia> implements FisioterapiaRepository{

}
