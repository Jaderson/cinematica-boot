package com.cinematica.repository.acao;

import com.cinematica.model.autenticacao.Acao;
import com.cinematica.repository.Repository;

public interface AcaoRepository extends Repository<Acao>{

}
