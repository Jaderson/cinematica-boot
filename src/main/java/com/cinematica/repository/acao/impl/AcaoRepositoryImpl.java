package com.cinematica.repository.acao.impl;

import org.springframework.stereotype.Component;

import com.cinematica.model.autenticacao.Acao;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.acao.AcaoRepository;

@Component
@SuppressWarnings("unchecked")
public class AcaoRepositoryImpl extends AbstractRepository<Acao> implements AcaoRepository{

}
