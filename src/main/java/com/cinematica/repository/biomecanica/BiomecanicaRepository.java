package com.cinematica.repository.biomecanica;

import com.cinematica.framework.avaliacoes.Biomecanica;
import com.cinematica.repository.Repository;

public interface BiomecanicaRepository extends Repository<Biomecanica>{

}
