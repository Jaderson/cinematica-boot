package com.cinematica.repository.biomecanica.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.avaliacoes.Biomecanica;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.biomecanica.BiomecanicaRepository;

@Component
@SuppressWarnings("unchecked")
public class BiomecanicaRepositoryImpl extends AbstractRepository<Biomecanica> implements BiomecanicaRepository{

}
