package com.cinematica.repository.acompanhamento.impl;

import org.springframework.stereotype.Component;

import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.repository.AbstractRepository;
import com.cinematica.repository.acompanhamento.AcompanhamentoRepository;

@Component
@SuppressWarnings("unchecked")
public class AcompanhamentoRepositoryImpl extends AbstractRepository<AcompanhamentoPessoa> implements AcompanhamentoRepository{

}
