package com.cinematica.repository.acompanhamento;

import com.cinematica.framework.acompanhamento.AcompanhamentoPessoa;
import com.cinematica.repository.Repository;

public interface AcompanhamentoRepository extends Repository<AcompanhamentoPessoa>{

}
