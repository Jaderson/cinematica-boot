package com.cinematica.model.annotations.relatorio;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
*<p>Anotação usada para geração de relatórios dinâmicos com DynamicJasper</p>
*<p>O atributo título é o título da coluna do relatório</p>
*<p>O atributo name é o caminho para chegar ao campo onde está o dado. Ex.: Atividade possui um Local e Local possui uma descrição => local.descricao</p>
* <p>Por padrão o atributo classname() é do tipo String. Se o tipo anotado for do tipo string não é 
* necessário usar este atributo</p>
* @author Thiago Ramos
* @since 20/02/2014
*
*/
@Target(METHOD)
@Retention(RUNTIME)
public @interface Relatorio {

	String titulo() default "";
	
	String name() default "";
	
	Class<?> className() default String.class;
	
	int tamanho() default 85;
	
	int ordenacao() default 1;
}
