package com.cinematica.model.autenticacao;

import java.util.List;

import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.pessoa.SimNao;

public class UsuarioDTO {

	private Integer id;
	private String nome;
	private Integer idPessoa;
	private String login;
	private String tokenAcesso;
	private SimNao ativo;
	private Empresa empresa;
	private List<String> authorities;

	public UsuarioDTO(Integer id, String nome, Integer idPessoa, String login, String tokenAcesso, SimNao ativo,
			Empresa empresa, List<String> authorities) {
		super();
		this.id = id;
		this.nome = nome;
		this.idPessoa = idPessoa;
		this.login = login;
		this.tokenAcesso = tokenAcesso;
		this.ativo = ativo;
		this.empresa = empresa;
		this.authorities = authorities;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getTokenAcesso() {
		return tokenAcesso;
	}

	public void setTokenAcesso(String tokenAcesso) {
		this.tokenAcesso = tokenAcesso;
	}

	public SimNao getAtivo() {
		return ativo;
	}

	public void setAtivo(SimNao ativo) {
		this.ativo = ativo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<String> getAuthorities() {
		return authorities;
	}
}
