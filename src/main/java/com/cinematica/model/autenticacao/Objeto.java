package com.cinematica.model.autenticacao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.cinematica.framework.contexto.Alterar;
import com.cinematica.framework.contexto.Cadastrar;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;


@Entity
@Audited
@Table(name = "objeto")
public class Objeto implements Serializable, Identidade, Ordenacao{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String descricao;
	
	public Objeto() {}
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "descricao", length = 200, nullable = false)
	@NotNull(message = "erro_descricao_nao_pode_ser_nula", groups = {Cadastrar.class, Alterar.class})
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	@Transient
	public String getCampoOrderBy() {
		return "descricao";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Objeto))
			return false;
		Objeto other = (Objeto) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}
}