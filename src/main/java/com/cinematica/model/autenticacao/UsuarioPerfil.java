package com.cinematica.model.autenticacao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.cinematica.framework.contexto.Alterar;
import com.cinematica.framework.contexto.Cadastrar;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;

@Entity
@Audited
@Table(name = "usuario_perfil")
public class UsuarioPerfil implements Serializable, Identidade, Ordenacao{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Usuario usuario;
	private Perfil perfil;
	
	public UsuarioPerfil() {}
	
	
	public UsuarioPerfil(Integer id, Usuario usuario, Perfil perfil) {
		this.id = id;
		this.usuario = usuario;
		this.perfil = perfil;
	}

	public UsuarioPerfil(Usuario usuario, Perfil perfil) {
		this.usuario = usuario;
		this.perfil = perfil;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "usuarios_id", nullable = false)
	@NotNull(message = "O Usuário não pode ser nulo.", groups = { Cadastrar.class, Alterar.class })
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@ManyToOne
	@JoinColumn(name = "perfil_id", nullable = false)
	@NotNull(message = "O Perfil não pode ser nulo.", groups = { Cadastrar.class, Alterar.class })
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
	@Override
	@Transient
	public String getCampoOrderBy() {
		return "perfil.descricao";
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UsuarioPerfil))
			return false;
		UsuarioPerfil other = (UsuarioPerfil) obj;
		if (perfil == null) {
			if (other.perfil != null)
				return false;
		} else if (!perfil.equals(other.perfil))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
}
