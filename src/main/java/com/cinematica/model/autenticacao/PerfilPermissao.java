package com.cinematica.model.autenticacao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.cinematica.framework.contexto.Alterar;
import com.cinematica.framework.contexto.Cadastrar;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;

@Entity
@Audited
@Table(name = "perfil_permissoes")
public class PerfilPermissao implements Serializable, Identidade, Ordenacao{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Perfil perfil;
	private Permissao permissao;

	public PerfilPermissao() {}
	
	public PerfilPermissao(Perfil perfil, Permissao permissao) {
		this.perfil = perfil;
		this.permissao = permissao;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "perfil_id", nullable = false)
	@NotNull(message = "O Perfil não pode ser nulo.", groups = { Cadastrar.class, Alterar.class })
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@ManyToOne
	@JoinColumn(name = "permissao_id", nullable = false)
	@NotNull(message = "A Permissão não pode ser nulo.", groups = { Cadastrar.class, Alterar.class })
	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	@Override
	@Transient
	public String getCampoOrderBy() {
		return "perfil.descricao";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result
				+ ((permissao == null) ? 0 : permissao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PerfilPermissao))
			return false;
		PerfilPermissao other = (PerfilPermissao) obj;
		if (perfil == null) {
			if (other.perfil != null)
				return false;
		} else if (!perfil.equals(other.perfil))
			return false;
		if (permissao == null) {
			if (other.permissao != null)
				return false;
		} else if (!permissao.equals(other.permissao))
			return false;
		return true;
	}

}
