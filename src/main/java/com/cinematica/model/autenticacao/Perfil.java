package com.cinematica.model.autenticacao;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.cinematica.framework.contexto.Alterar;
import com.cinematica.framework.contexto.Cadastrar;
import com.cinematica.model.empresa.Empresa;
import com.cinematica.model.empresa.SetadorEmpresa;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;

@Entity
@Audited
@Table(name = "perfil")
public class Perfil implements Serializable, Identidade, Ordenacao, SetadorEmpresa{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String descricao;
	private Empresa empresa;

	private Set<PerfilPermissao> permissoes = new HashSet<PerfilPermissao>();
	
	public Perfil() {}
	
	public Perfil(Integer id, String descricao, Empresa empresa, Set<PerfilPermissao> permissoes) {
		this.id = id;
		this.descricao = descricao;
		this.empresa = empresa;
		this.permissoes = permissoes;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "descricao", nullable = false)
	@NotNull(message = "erro_descricao_nao_pode_ser_nula", groups = { Cadastrar.class, Alterar.class })
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@ManyToOne
	@JoinColumn(name = "empresas_id", nullable = false)
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "perfil")
	public Set<PerfilPermissao> getPermissoes() {
		return permissoes;
	}
	
	public void setPermissoes(Set<PerfilPermissao> permissoes) {
		this.permissoes = permissoes;
	}
	
	@Override
	@Transient
	public String getCampoOrderBy() {
		return "descricao";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Perfil))
			return false;
		Perfil other = (Perfil) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
