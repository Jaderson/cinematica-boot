package com.cinematica.model.especialidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.cinematica.model.annotations.relatorio.Relatorio;
import com.cinematica.model.identidade.Identidade;
import com.cinematica.model.ordenacao.Ordenacao;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Audited
@Table(name = "especialidade")
public class Especialidade implements Serializable,Identidade,Ordenacao{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String descricao;

	public Especialidade() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="descricao", nullable=false)
	@NotNull
	@Relatorio(name="descricao", tamanho=250, titulo="", ordenacao=1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Especialidade other = (Especialidade) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	@Transient
	@JsonIgnoreProperties(ignoreUnknown = true)
	public String getCampoOrderBy() {
		return "descricao";
	}

}
