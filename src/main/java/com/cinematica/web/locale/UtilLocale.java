package com.cinematica.web.locale;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UtilLocale {

	/*private static final String BASE_PATH_SERVIDOR = "//Users//"+System.getProperty("user.name")+"//fisiosystem";
	private static final String BASE_PASTA_SERVIDOR = "//fisio-web//src//main//java//com//fisio//web//messages//language_pt_BR.properties"; */   
	
	private static final String BASE_PATH_SERVIDOR = "//home//cinemat///appservers//apache-tomcat-7.0.62//webapps";
	private static final String BASE_PASTA_SERVIDOR = "//fisio-web//WEB-INF//classes//com//fisio//web//messages//language_pt_BR.properties";  
  

	public static String getMensagemI18n(String chave) throws IOException {
		String msg = getProp().getProperty(chave.trim());
		return msg;
	}

	public static Properties getProp() throws IOException {
		Properties props = new Properties();
		FileInputStream file = new FileInputStream(BASE_PATH_SERVIDOR + BASE_PASTA_SERVIDOR); 
		props.load(file);
		return props;
	}
}
