package com.cinematica.web.constantes;

public interface ConstantesUtilWeb {
	
	String REGISTRO_CADASTRADO_COM_SUCESSO = "registro_cadastrado_com_sucesso";
	String REGISTRO_ALTERADO_COM_SUCESSO = "registro_alterado_com_sucesso";
	String REGISTRO_EXCLUIDO_COM_SUCESSO = "registro_excluido_com_sucesso";

	String MENSAGEM_SUCESSO = "Sucesso";
	String MENSAGEM_AVISO = "Aviso Cuidado";
	String MENSAGEM_ALERTA = "Alerta Cuidado";
	String MENSAGEM_ERRO = "Erro Cuidado";
	String MENSAGEM_ERRO_FATAL = "O erro fatal";
}
